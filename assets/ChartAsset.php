<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.09.2016
 * Time: 11:30
 */

namespace app\assets;


use yii\web\AssetBundle;

class ChartAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl  = '@web';

    public $css = [
        'css/handle-chart.less',
    ];

    public $js = [
        'js/handle-chart.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'dosamigos\chartjs\ChartJsAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}