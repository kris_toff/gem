<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.09.2016
 * Time: 12:22
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\ContactInfo;
use app\models\ContactForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\bootstrap\Html;

class ContactsController extends Controller {

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionIndex() {
        $model = new ContactForm();

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Your message with <strong>ID {id}</strong> has been sent successfully.', ['id' => $model->id]));

                return $this->refresh();
            } else {
                $errorStr = Html::tag('h4', \Yii::t('app', 'Errors in the message:'));
                foreach ($model->getErrors() as $name => $error) {
                    $errorStr .= Html::tag('p', \Yii::t('app', 'For input {label}:', ['label' => $model->getAttributeLabel($name)]));
                    $errorStr .= Html::ul($error);
                }

                \Yii::$app->session->setFlash('error',$errorStr);
            }
        }

        // поля контактных данных
        $modelContact = ContactInfo::find()->where(['active' => 1])->all();
        $contacts = ArrayHelper::toArray($modelContact, [
            ContactInfo::className() => [
                'icon',
                'text',
            ],
        ]);

        return $this->render('index', [
            'model'    => $model,
            'contacts' => $contacts,
        ]);
    }

}