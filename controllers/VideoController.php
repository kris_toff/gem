<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 23:42
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\Video;
use app\models\AR\VideoTabs;
use app\models\SearchModule;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;

class VideoController extends Controller {
    public $defaultAction = 'list';

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionList($tab = 0) {
        $tabsA = VideoTabs::find()->indexBy('id')->all();

        $request = \Yii::$app->request;

        $modelSearch = new SearchModule();
        $modelSearch->load($request->get());

        $isSearch = false;

        if ($tab < 1 || !ArrayHelper::keyExists($tab, $tabsA)) {
            $ids = ArrayHelper::getColumn($tabsA, 'active');
            $tab = (count($ids)>0) ? array_search(1, $ids, true) : array_rand($tabsA);
            $request->setQueryParams(array_merge($request->getQueryParams(), ['tab' => $tab]));
        }
        if ($modelSearch->validate()) {
            $isSearch = true;
            $tabsA[] = $modelSearch;
        }

        $tabs = ArrayHelper::toArray($tabsA, [
            VideoTabs::className() => [
                'label' => function($model){
                    return \Yii::t('app', $model->label);
                },
                'content'     => function ($model) use ($tab, $isSearch) {
                    return (!$isSearch && $tab == $model->id) ? $this->getRibbonVideos($model->id) : 'Loading...';
                },
                'active'      => function ($model) use ($tab, $isSearch) {
                    return boolval(!$isSearch && $tab == $model->id );
                },
                'linkOptions' => function ($model) {
                    return [
                        'data-url' => Url::toRoute(['shift', 'tab' => $model->id]),
                    ];
                },
            ],
            SearchModule::className() => [
                'label' => function($model){
                    return \Yii::t('app' , 'Search result');
                },
                'content' => function($model)use($tab){
                    return $this->getRibbonVideos($tab, $model);
                },
                'active' => function(){
                    return true;
                },
                'linkOptions' => function(){
                    return [
                        'data-close' => 'always',
                    ];
                }
            ]
        ]);

        return $this->render('list', [
            'tabs' => $tabs,
        ]);
    }

    protected function getRibbonVideos($tab_num, SearchModule $searchModel = null) {
        $query = Video::find()->where(['tab_id' => $tab_num]);
        if ($searchModel != null){
            $query->andFilterWhere([
                'or',
                ['like', 'name', $searchModel->text],
                ['like', 'label', $searchModel->text]
            ]);
        }

        $provider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSizeLimit' => [5, 12],
                'pageSize'      => 4,
                'pageParam'     => 'video-page',
                'pageSizeParam' => 'video-per-page',
                'route'         => 'video/list',
            ],
            'sort'       => [
                'sortParam'       => 'video-sort',
                'separator'       => ',',
                'enableMultiSort' => true,
                /**/
                'attributes'      => [
                    'standard' => [
                        'asc'     => ['created_at' => SORT_ASC, 'updated_at' => SORT_DESC],
                        'desc'    => ['created_at' => SORT_DESC, 'updated_at' => SORT_ASC],
                        'label'   => Inflector::camel2words('byDate'),
                        'default' => SORT_DESC,
                    ],

                ],
                'defaultOrder'    => [
                    'standard' => SORT_DESC,
                ],
                /**/
            ],
        ]);

        return $this->renderPartial('list/list-videos', [
            'tab'          => $tab_num,
            'dataProvider' => $provider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionShift($tab) {
        return Json::encode($this->getRibbonVideos($tab));
    }
}