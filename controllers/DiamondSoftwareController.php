<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 23:24
 */

namespace app\controllers;

use app\components\behaviors\BMenu;
use app\models\AR\DiamondCut;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\AR\DiamondPrice;

class DiamondSoftwareController extends Controller {
    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionIndex() {
        $dcut = DiamondCut::find()->all();
//        $dcut = (new Query())->from('{{diamond_cut}}')->all();
        $dcutmap = ArrayHelper::map($dcut, 'id', 'name');
        $dcutBtn = [];
        $dcutActiveNum = array_rand($dcut);
        $cut_id = 0;
        foreach ($dcut as $key => $item) {
            $lwc = strtolower(strtr($item['name'], [' ' => '']));
            $dcutBtn[] = [
                'label'   => \Yii::t('app', $item['name']),
                'options' => [
                    'id'        => $lwc . '-graph',
                    'data-type' => $item['id'],
                    'class'     => ($key == $dcutActiveNum) ? 'active' : '',
                ],
            ];
        }

        $dcarat = (new Query())->from('{{diamond_carat}}')->all();
        $dcaratmap = ArrayHelper::map($dcarat, 'id', 'name');
        $dcaratActiveNum = array_rand($dcarat);

        $dclarity = (new Query())->from('{{diamond_clarity}}')->all();
        $dclaritymap = ArrayHelper::map($dclarity, 'id', 'label');
        $dclarityActiveNum = array_rand($dclarity);

        $dcolor = (new Query())->from('{{diamond_color}}')->all();
        $dcolormap = ArrayHelper::map($dcolor, 'id', 'label');
        $dcolorActiveNum = array_rand($dcolor);

        $dprice = DiamondPrice::find()->asArray()->all();


        $result = GraphicsController::generateData($dcut[ $dcutActiveNum ]['id'], $dcarat[ $dcaratActiveNum ]['id'], $dclarity[ $dclarityActiveNum ]['id'], $dcolor[ $dcolorActiveNum ]['id']);

        return $this->render('index', [
            'labels'      => ArrayHelper::getValue($result, 'labels', []),
            'values'      => ArrayHelper::getValue($result, 'values', []),
            'title'       => ArrayHelper::getValue($result, 'title', ''),
            'dcutmap'     => $dcutmap,
            'dcaratmap'   => $dcaratmap,
            'dclaritymap' => $dclaritymap,
            'dcolormap'   => $dcolormap,
            'dcutBtn'     => $dcutBtn,
        ]);
    }
}