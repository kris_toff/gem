<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 17:56
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use yii\helpers\Json;
use yii\web\Controller;

class TestController extends Controller{

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionUpdate(){
        return Json::encode('sdknskgnfdg');
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}