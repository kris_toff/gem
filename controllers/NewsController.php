<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 17:03
 */

namespace app\controllers;

use app\components\behaviors\BMenu;
use app\components\wizard\searchMini\SearchMiniWidget;
use app\models\AR\News;
use app\models\AR\NewsCategory;
use app\models\AR\NewsTabs;
use app\models\NewsSearch;
use app\models\SearchModule;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class NewsController extends Controller {
    public $defaultAction = 'list';

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionList($tab = 0, $category = 0) {
        $tabsA = NewsTabs::find()->where(['active' => 1])->indexBy('id')->all();

        $modelSearch = new SearchModule();
        $modelSearch->load(\Yii::$app->request->get());
        $isSearch = false;

        $request = \Yii::$app->request;

        if ($tab < 1 || !ArrayHelper::keyExists($tab, $tabsA)) {
            $ids = ArrayHelper::getColumn($tabsA, 'active');
            $tab = (count($ids) > 0) ? array_search(1, $ids, true) : array_rand($tabsA);
            $request->setQueryParams(array_merge($request->getQueryParams(), ['tab' => $tab]));
        }
        if ($modelSearch->validate()) {
            $isSearch = true;
            $tabsA[] = $modelSearch;
        }

        $tabs = ArrayHelper::toArray($tabsA, [
            NewsTabs::className()     => [
                'label'       => function ($model) {
                    return \Yii::t('app', $model->label);
                },
                'content'     => function ($model) use ($tab, $category, $isSearch) {
                    return (!$isSearch
                        && $tab
                        == $model->id) ? $this->getRibbonNews($model->id, $category) : 'Loading...';
                },
                'active'      => function ($model) use ($tab, $isSearch) {
                    return boolval(!$isSearch && $tab == $model->id);
                },
                'linkOptions' => function ($model) {
                    return [
                        'data-url' => Url::toRoute(['shift', 'tab' => $model->id]),
                    ];
                },
            ],
            SearchModule::className() => [
                'label'       => function () { return \Yii::t('app', 'Search result'); },
                'content'     => function ($model) use ($tab, $category) {
                    return $this->getRibbonNews($tab, $category, $model);
                },
                'active'      => function () {
                    return true;
                },
                'linkOptions' => function () {
                    return [
                        'data-close' => 'always',
                    ];
                },
            ],
        ]);

        return $this->render('list', [
            'tabs' => $tabs,
        ]);
    }

    /**
     * @param int                           $tab_num
     * @param int                           $category
     * @param \app\models\SearchModule|null $searchModel
     *
     * @return string
     */
    public function getRibbonNews($tab_num, $category = 0, SearchModule $searchModel = null) {
        $query = News::find();
        if ($category > 0) {
            $query->where(['category_id' => $category]);
        } else {
            $query->where(['category_id' => NewsCategory::find()->select('id')->where(['tab_id' => $tab_num])]);
        }

        if ($searchModel != null) {
            $query->andFilterWhere([
                'or',
                ['like', 'name', $searchModel->text],
                ['like', 'title', $searchModel->text],
                ['like', 'preview', $searchModel->text],
                ['like', 'content', $searchModel->text],
            ]);
        }

        $dataProviderNews = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSizeLimit' => [10, 25],
                'pageSize'      => 4,
                'pageParam'     => 'news-page',
                'pageSizeParam' => 'news-per-page',
                'route'         => 'news/list',
            ],
            'sort'       => [
                'sortParam'    => 'news-sort',
                'attributes'   => [
                    'standard' => [
                        'asc'     => ['created_at' => SORT_ASC, 'updated_at' => SORT_DESC],
                        'desc'    => ['created_at' => SORT_DESC, 'updated_at' => SORT_ASC],
                        'label'   => Inflector::camel2words('byDate'),
                        'default' => SORT_DESC,
                    ],

                ],
                'defaultOrder' => [
                    'standard' => SORT_DESC,
                ],
            ],
        ]);

        $categoriesA = NewsCategory::find()->where(['tab_id' => $tab_num])->with('news')->all();
        $categories = ArrayHelper::toArray($categoriesA, [
            NewsCategory::className() => [
                'label' => 'name',
                'badge' => function ($model) {
                    return count($model->news);
                },
                'url'   => function ($model) use ($tab_num) {
                    return Url::to(['list', 'tab' => $tab_num, 'category' => $model->id]);
                },
            ],
        ]);

        $popularDataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'page'     => 0,
                'pageSize' => 6,
            ],
            'sort'       => [
                'attributes'   => [
                    'standard' => [
                        'desc'    => ['views' => SORT_DESC, 'created_at' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => [
                    'standard' => SORT_DESC,
                ],
            ],
        ]);

        return $this->renderPartial('list/list-news', [
            'dataProvider' => $dataProviderNews,
            'categories'   => $categories,
            'tab'          => $tab_num,
            'mostPopular'  => $popularDataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    public function actionShift($tab) {
        return Json::encode($this->getRibbonNews($tab));
    }

    public function actionStory($id) {
        if ($id < 1) {
            throw new BadRequestHttpException('ID must be greater then 0.');
        }

        $model = News::find()->where(['id' => $id])->with(['category', 'category.tab', 'relatedList'])->one();
        if ($model == null) {
            throw new BadRequestHttpException("News with id {$id} do not exists.");
        }

        $tab_id = $model->category->tab->id;
        $categoriesA = NewsCategory::find()->where(['tab_id' => $tab_id])->with('news')->all();
        $categories = ArrayHelper::toArray($categoriesA, [
            NewsCategory::className() => [
                'label' => 'name',
                'badge' => function ($model) {
                    return count($model->news);
                },
                'url'   => function ($model) use ($tab_id) {
                    return Url::to(['list', 'tab' => $tab_id, 'category' => $model->id]);
                },
            ],
        ]);

        $relatedListA = $model->relatedList;
        $related_ids = ArrayHelper::getColumn($relatedListA, 'related_news_id');

        $relatedNewsProvider = new ActiveDataProvider([
            'query'      => News::find()->where(['id' => $related_ids]),
            'pagination' => [
                'pageSize' => 4,
            ],
            'sort'       => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $model->updateCounters(['views' => 1]);

        return $this->render('view', [
            'model'      => $model,
            'categories' => $categories,
            'related'    => $relatedNewsProvider->getModels(),
        ]);
    }
}