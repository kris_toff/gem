<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 11:23
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\MapPoints;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class DiamondMapController extends Controller{

    public function behaviors(){
        return [
            BMenu::className(),
        ];
    }

    public function actionIndex(){
        $pointsA = MapPoints::findAll(['active' => 1]);
        $points = ArrayHelper::toArray($pointsA, [
            MapPoints::className() => [
                'tooltip',
                'title',
                'text',
                'latitude',
                'longitude',

            ]
        ]);


        return $this->render('index', [
            'points' => $points
        ]);
    }
}