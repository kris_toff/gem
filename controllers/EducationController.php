<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 2:30
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\Education;
use app\models\AR\EducationMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EducationController extends Controller {
    public $defaultAction = 'list';

    public function actionList() {
        $menuA = EducationMenu::find()->with('lessons')->indexBy('id')->all();
        $menu = ArrayHelper::toArray($menuA, [
            EducationMenu::className() => [
                'url'    => function () { return '#'; },
                'label',
                'active' => function ($model) {
                    return false;
                },
            ],
        ]);

        $lessonsA = Education::find()->all();
        $lessons = ArrayHelper::toArray($lessonsA, [
            Education::className() => [
                'label' => 'name',
                'url'   => function ($model) { return Url::toRoute(['lesson', 'id' => $model->id]); },
                'menu_id',
            ],
        ]);

        foreach ($lessons as $key => $lesson) {
            $mid = $lesson['menu_id'];
            unset($lesson['menu_id']);

            $menu[ $mid ]['items'][] = $lesson;
        }

        $defaultMenu = array_shift($menuA);
        $menu[ $defaultMenu->id ]['active'] = true;

        return $this->render('list', [
            'menu'    => $menu,
            'model'   => $defaultMenu,
            'lessons' => ArrayHelper::getValue($defaultMenu, 'lessons', []),
        ]);
    }

    public function actionLesson($id) {
        $id = intval($id);
        if ($id < 1) {
            throw new BadRequestHttpException('Wrong ID lesson.');
        } elseif (($model = Education::findOne($id)) == null) {
            throw new NotFoundHttpException("Lesson with ID {$id} not found.");
        }

        $menuA = EducationMenu::find()->with('lessons')->indexBy('id')->all();
        $menu = ArrayHelper::toArray($menuA, [
            EducationMenu::className() => [
                'url' => function () { return '#'; },
                'label',
            ],
        ]);

        $lessonsA = Education::find()->all();
        $lessons = ArrayHelper::toArray($lessonsA, [
            Education::className() => [
                'label'  => 'name',
                'url'    => function ($model) { return ['lesson', 'id' => $model->id]; },
                'menu_id',
                'active' => function ($model) use ($id) {
                    return ($model->id === $id);
                },
            ],
        ]);

        foreach ($lessons as $key => $lesson) {
            $mid = $lesson['menu_id'];
            unset($lesson['menu_id']);

            $menu[ $mid ]['items'][] = $lesson;
        }

        return $this->render('view', [
            'model' => $model,
            'menu'  => $menu,
        ]);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}
