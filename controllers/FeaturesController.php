<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.09.2016
 * Time: 10:20
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\components\behaviors\BTrend;
use yii\web\Controller;

class FeaturesController extends Controller{

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    /**
     * Главная страница вкладки features
     */
    public function actionIndex(){

        return $this->render('index');
    }
}