<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 17.09.2016
 * Time: 1:59
 */

namespace app\controllers;

use app\models\AR\DiamondCarat;
use app\models\AR\DiamondClarity;
use app\models\AR\DiamondColor;
use app\models\AR\DiamondCut;
use app\models\AR\DiamondPrice;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

class GraphicsController extends Controller {
    public static function generateData($cut = 0, $carat = 0, $clarity = 0, $color = 0) {
        $curL = DiamondCut::find()->select('id')->column();
        $cut = ($cut > 0 && ArrayHelper::isIn($cut, $curL)) ? $cut : ArrayHelper::getValue($curL, array_rand($curL), 0);
        if (!$cut) throw new BadRequestHttpException("No cut with id {$cut}.");

        $caratL = (new Query())->select('diamond_carat_rid')->from('{{diamond_price}}')->where(['diamond_cut_rid' => $cut])->distinct()->column();
        $carat = ($carat > 0 && ArrayHelper::isIn($carat, $caratL)) ? $carat : ArrayHelper::getValue($caratL, array_rand($caratL), 0);
        if (!$carat) throw new BadRequestHttpException("No carat with id {$carat}.");

        $clarityL = (new Query())->select('diamond_clarity_rid')->from('{{diamond_price}}')->where(['diamond_cut_rid' => $cut, 'diamond_carat_rid' => $carat])->distinct()->column();
        $clarity = ($clarity > 0 && ArrayHelper::isIn($clarity, $clarityL)) ? $clarity : ArrayHelper::getValue($clarityL, array_rand($clarityL), 0);
        if (!$clarity) throw new BadRequestHttpException("No clarity with id {$clarity}.");

        $colorL = (new Query())->select('diamond_color_rid')->from('{{diamond_price}}')->where(['diamond_cut_rid' => $cut, 'diamond_carat_rid' => $carat, 'diamond_clarity_rid' => $clarity])->distinct()->column();
        $color = ($color > 0 && ArrayHelper::isIn($color, $colorL)) ? $color : ArrayHelper::getValue($colorL, array_rand($colorL), 0);
        if (!$color) throw new BadRequestHttpException("No color with id {$color}.");

        $cDate = new \DateTime();
        $cDate->sub(new \DateInterval('P18M'));

        $priceData = DiamondPrice::find()
            ->where([
                'diamond_cut_rid'     => $cut,
                'diamond_carat_rid'   => $carat,
                'diamond_clarity_rid' => $clarity,
                'diamond_color_rid'   => $color,
            ])
            ->andWhere(['>=', 'date', $cDate->getTimestamp()])
            ->orderBy(['date' => SORT_ASC, ])
            ->all();

        $labels = [];
        $values = [];
        /** @var DiamondPrice $item */
        foreach ($priceData as $item) {
            $dl = (new \DateTime())->setTimestamp($item->date);
            $labels[] = "{$dl->format('m')}/{$dl->format('y')}";
            $values[] = $item->cost;
        }

        $label = \Yii::t('app', 'Diamond Price Graph:'). ' ';
        $label .= DiamondCut::find()->select('name')->where(['id' => $cut])->scalar() . ', ';
        $label .= DiamondCarat::find()->select('name')->where(['id' => $carat])->scalar() . ', ';
        $label .= DiamondClarity::find()->select('label')->where(['id' => $clarity])->scalar(). ', ';
        $label .= DiamondColor::find()->select('label')->where(['id' => $color])->scalar() . '.';

        return [
            'labels' => $labels,
            'values' => $values,
            'title' => $label,
        ];
    }

    public function behaviors() {
        return [
            /**/
            'varbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'generate-line' => ['post'],
                ],
            ],
            /**/
            [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    'text/html'        => Response::FORMAT_HTML,
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionGenerateLine() {
        $config_path = realpath(\Yii::getAlias('@app/views/diamond-software/parts/config.inc.php'));
        $request = \Yii::$app->request;

        $result = self::generateData($request->post('cut', 0), $request->post('carat', 0), $request->post('clarity', 0), $request->post('color', 0));
//print_r($result);exit;
        $labels = $result['labels'];
        $data_values = $result['values'];
        $config = require($config_path);

        $id = $config['options']['id'];
        $config['options'] = $config['clientOptions'];
        unset($config['clientOptions']);

        return [
            'config' => $config,
            'id'     => $id,
            'title'  => $result['title'] ,
        ];
    }
}