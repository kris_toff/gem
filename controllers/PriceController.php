<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 12:31
 */

namespace app\controllers;


use app\models\AR\DiamondPrice;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class PriceController extends Controller {
    public function beforeAction($action) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }

    public function actionCalculate() {
        $model = new DiamondPrice(['scenario' => DiamondPrice::SCENARIO_CALCULATE]);
        $model->attributes = \Yii::$app->request->post();

        $result = $model->calculate();

        if ($result === false) {
            throw new ServerErrorHttpException('no data');
        }

        return array_merge(['status' => 'ok'], $result);
    }
}