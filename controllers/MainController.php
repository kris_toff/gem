<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.09.2016
 * Time: 12:20
 */

namespace app\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\DiamondCut;
use app\models\AR\NewsCategory;
use app\models\AR\NewsTabs;
use app\models\AR\SettingsSlider;
use app\models\NewsManager;
use app\modules\admin\models\SettingsParam;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorHandler;

class MainController extends Controller {
    public function actions() {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionIndex() {
        $sliderData = SettingsSlider::find()->all();
        $sliderItems = ArrayHelper::toArray($sliderData, [
            SettingsSlider::className() => [
                'href'    => function ($model) {
                    return Url::toRoute([$model->src]);
                },
                'options' => function ($model) {
                    return [
                        'data-bgtext' => $model->big_text,
                        'data-smtext' => $model->small_text,
                        'data-url'    => $model->url,
                    ];
                },
            ],
        ]);

        $params = new SettingsParam();

        // для графика
        $dcut = DiamondCut::find()->all();
//        $dcut = (new Query())->from('{{diamond_cut}}')->all();
        $dcutmap = ArrayHelper::map($dcut, 'id', 'name');
        $dcutBtn = [];
        $dcutActiveNum = array_rand($dcut);
        $cut_id = 0;
        foreach($dcut as $key => $item){
            $lwc = strtolower(strtr($item['name'], [' ' => '']));
            $dcutBtn[] = [
                'label' => $item['name'],
                'options' => [
                    'id' => $lwc . '-graph',
                    'data-type' => $item['id'],
                    'class' => ($key == $dcutActiveNum) ? 'active' : '',
                ]
            ];
        }


        $dcarat = (new Query())->from('{{diamond_carat}}')->all();
        $dcaratmap = ArrayHelper::map($dcarat, 'id', 'name');
        $dcaratActiveNum = array_rand($dcarat);

        $dclarity = (new Query())->from('{{diamond_clarity}}')->all();
        $dclaritymap = ArrayHelper::map($dclarity, 'id', 'label');
        $dclarityActiveNum = array_rand($dclarity);

        $dcolor = (new Query())->from('{{diamond_color}}')->all();
        $dcolormap = ArrayHelper::map($dcolor, 'id', 'label');
        $dcolorActiveNum = array_rand($dcolor);


        $result = GraphicsController::generateData($dcut[$dcutActiveNum]['id'], $dcarat[$dcaratActiveNum]['id'],$dclarity[$dclarityActiveNum]['id'],$dcolor[$dcolorActiveNum]['id']);

        // новости
        $newsC = NewsManager::random();
//print '<pre>';        print_r($newsC); exit;
        return $this->render('index', [
            'slItems' => $sliderItems,
            'happiest_user' => rand($params->limit_users_min, $params->limit_users_max),
            'labels' => ArrayHelper::getValue($result, 'labels', []),
            'values' => ArrayHelper::getValue($result, 'values', []),
            'title' => ArrayHelper::getValue($result, 'title', ''),
            'dcutmap' => $dcutmap,
            'dcaratmap' => $dcaratmap,
            'dclaritymap' => $dclaritymap,
            'dcolormap' => $dcolormap,
            'dcutBtn' => $dcutBtn,
            'news' => ArrayHelper::getValue($newsC, 'list', []),
            'news_tab' => ArrayHelper::getValue($newsC, 'tab',0),
        ]);
    }
}