<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 23:26
 */

use yii\helpers\Url;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\Html;
use app\components\wizard\topic\TopicWidget;
use app\assets\ChartAsset;

/** @var \yii\web\View $this */

ChartAsset::register($this);

/** @var string[] $labels */
/** @var float[] $values */
/** @var string $title */
/** @var array $dcutBtn */

?>

<div class="wrap">
    <?php
    print TopicWidget::widget([
        'title' => 'Transparent diamond software',
    ])
    ?>

    <div class="row">
        <p class="text-muted text-center"><?= \Yii::t('app', 'Select the diamond, carat, clarity and color to view the current price') ?></p>
    </div>

    <div class="row chart-wind text-center">
        <div class="col-sm-12">
            <?= ButtonGroup::widget([
                'id'      => 'switch-graph-group',
                'buttons' => $dcutBtn,
                'options' => [
                    'class' => 'simple-line',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <p id="graph-title"><?= $title; ?></p>
        </div>
    </div>
    <div class="row" id="chart-graph-frame">
        <?= $this->render('parts/graph', ['labels' => $labels, 'data_values' => $values]); ?>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="panel-handle-diamond-price" class="col-sm-12 bg-white"
                 style="box-shadow: 0 0 8px 0 rgba(200, 150, 100, 0.4);">
                <p class="lead text-center"><strong><?= \Yii::t('app', 'Diamond price calculator') ?></strong></p>

                <div class="col-md-2">
                    <?= $this->render('parts/_droplist', [
                        'id'    => 'diamond_cut_rid',
                        'label' => \Yii::t('app', 'Diamond cut'),
                        'items' => $dcutmap,
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <?= $this->render('parts/_droplist', [
                        'id'    => 'diamond_carat_rid',
                        'label' => \Yii::t('app', 'Carat weight'),
                        'items' => $dcaratmap,
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <?= $this->render('parts/_droplist', [
                        'id'    => 'diamond_clarity_rid',
                        'label' => \Yii::t('app', 'Clarity'),
                        'items' => $dclaritymap,
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <?= $this->render('parts/_droplist', [
                        'id'    => 'diamond_color_rid',
                        'label' => \Yii::t('app', 'Color'),
                        'items' => $dcolormap,
                    ]) ?>
                </div>
                <div class="col-md-2"><?= Html::button(\Yii::t('app', 'Calculate'), [
                        'class' => 'btn btn-success btn-block btn-round',
                        'id'    => 'calculate-price-btn',
                    ]); ?></div>
                <div class="col-md-2">
                    <div class="today-price">
                        <p><strong><?= \Yii::t('app', "Today's price") ?></strong></p>
                        <p class="h4 price-sence">$ <span id="diamond-today-price">0.00</span></p>
                    </div>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>