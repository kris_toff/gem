<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 11:53
 */

use dosamigos\chartjs\ChartJs;

/** @var \yii\web\View $this */
?>

<?php
/*
 {
"type":"line",
"data":{
"labels":["3/16","4/16","3/16","4/16","5/16","6/16","7/16","8/16","9/16","10/16","11/16","12/16"],
"datasets":[{"fillColor":"rgba(220,220,220,0.5)","strokeColor":"rgba(220,220,220,1)","pointColor":"rgba(220,220,220,1)","pointStrokeColor":"#fff","data":[65,59,10,15]}]
},
"options":{
"scales":{
"yAxes":[{"ticks":{"beginAtZero":true},"gridLines":{"display":false}},{"stacked":true}],"xAxes":[{"stacked":true}]
},
"legend":{"display":false,"labels":{"fontColor":"rgb(255, 99, 132)"}}
}
}
 */
$config = require( __DIR__ . '/config.inc.php');
?>

<div class="col-sm-10 col-sm-offset-1">
    <?= ChartJs::widget($config); ?>
</div>

