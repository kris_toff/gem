<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.09.2016
 * Time: 22:05
 */

use yii\bootstrap\Html;

/** @var string $label */
/** @var string $id */
/** @var array $items */


print Html::label($label, $id, ['class' => 'control-label']);

print Html::dropDownList($id, null, $items, [
    'class'       => 'form-control choise-group',
    'id'          => $id,
    'prompt' => \Yii::t('app', 'Please select'),
    'data-toggle' => 'popover',
    'data-trigger' => 'manual',
    'data-content' => \Yii::t('app', 'Not selected!'),
    'data-placement' => 'top',
]);

?>