<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.09.2016
 * Time: 17:48
 */

/*
 {"type":"line","data":{"labels":["3/16","4/16","3/16","4/16","5/16","6/16","7/16","8/16","9/16","10/16","11/16","12/16"],"datasets":[{"fillColor":"rgba(220,220,220,0.5)","strokeColor":"rgba(220,220,220,1)","pointColor":"rgba(220,220,220,1)","pointStrokeColor":"#fff","data":[65,59,10,15]}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true},"gridLines":{"display":false}},{"stacked":true}],"xAxes":[{"stacked":true}]},"legend":{"display":false,"labels":{"fontColor":"rgb(255, 99, 132)"}}}}
 */

/** @var string[] $labels */
/** @var float[] $data_values */

return [
    'type'          => 'line',
    'options'       => [
        'id'     => 'diamondChartGraph',
        'height' => 140,
        'width'  => 620,
    ],
    'clientOptions' => [
        'scales' => [
            'yAxes' => [
                [
                    'ticks'     => [
                        'beginAtZero' => true,
                    ],
                    'gridLines' => [
                        'display' => false,
                    ],
                ],
                [
                    'stacked' => true,
                ],
            ],
            'xAxes' => [
                [
                    'stacked' => true,

                ],
            ],
        ],
        'legend' => [
            'display' => false,
        ],
    ],
    'data'          => [
        'labels'   => $labels,
        'datasets' => [
            [
//                'fillColor'        => "rgba(207,240,230,1)",
//                'strokeColor'      => "rgba(77,215,140,1)",
//                'pointColor'       => "rgba(20,120,220,1)",

                'pointStrokeColor' => "#f00",
                'data'             => $data_values,
            ],
        ],
    ],
];