<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.09.2016
 * Time: 12:23
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\components\AlertWidget as Flash;

/** @var \yii\web\View $this */
/** @var \app\models\ContactForm $model */

?>

    <div class="bg-image">

        <?= Flash::widget() ?>

        <div class="row">
            <div class="wrapper-space">
                <div class="col-sm-12">
                    <div class="box-table bg-sky">
                        <div class="col-sm-8 bg-white">
                            <?= $this->render('parts/_form-feedback.php', ['model' => $model]); ?>
                        </div>
                        <div class="col-sm-4 bg-sky">
                            <?= $this->render('parts/_contacts.php', ['contacts' => $contacts]); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

<?php
$css = <<<CSS
.wrapper{
background-clip:border-box;
background-color:inherit;
background-origin:border-box;
background-position: left bottom;
background-repeat:no-repeat;
background-size:100% 80%;
background-attachment:scroll;
background-image:url('/img/Shape_2.png'),url('/img/Shape_1.png');
background-blend-mode: normal;
}
CSS;

$this->registerCss($css);