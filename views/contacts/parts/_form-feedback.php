<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.09.2016
 * Time: 13:59
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \app\models\ContactForm $model */
?>

<div class="box-info">
    <div class="box-info-header">
        <p class="h4"><strong><?= \Yii::t('app', 'Contact Us') ?></strong></p>
    </div>
    <div class="box-info-stuff">
        <?php $form = ActiveForm::begin([
            'id' => 'reply-letter',
            'validateOnChange' => false,
            'validateOnBlur' => true,
            'validateOnSubmit' => true,
            'fieldConfig' => [
                'enableLabel' => false,
            ],
            'options' => [
                'class' => 'simple-form',
            ]
        ]); ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['placeholder' => $model->getAttributeLabel('name')]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')]); ?>
            </div>
        </div>
        <?= $form->field($model, 'phone')->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'data-toggle' => 'popover', 'data-container' => 'body', 'data-trigger' => 'focus', 'data-placement' => 'bottom', 'data-content' => \Yii::t('app', 'Phone number must be next format:').' +38(095) 123 45 67']); ?>
<?//= $form->field($model, 'phone')->textInput(['placeholder' => $model->getAttributeLabel('phone'), ]); ?>
        <?= $form->field($model, 'body')->textarea(['placeholder' => $model->getAttributeLabel('body'), 'rows' => 3]); ?>

        <?= Html::submitButton(\Yii::t('app', 'send message'), ['class' => 'btn btn-success center-block', ]); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
