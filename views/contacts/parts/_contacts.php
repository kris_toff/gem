<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.09.2016
 * Time: 14:00
 */

/** @var array $contacts */
?>


<div class="box-info">
    <div class="box-info-header">
        <p class="h4"><?= \Yii::t('app', 'Contact Information') ?></p>
    </div>
    <div class="box-info-stuff">
        <ul class="media-list">
            <?php foreach ($contacts as $cont): ?>

                <?= $this->render('_item_contact', ['info' => $cont]) ?>

            <?php endforeach; ?>
        </ul>

    </div>
</div>
