<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 12:41
 */

use yii\bootstrap\Html;

/** @var array $info */

?>

<li class="media">
    <div class="media-left media-top">
        <?php if (!empty($info['icon'])) print Html::img($info['icon'], ['class' => 'media-object']); ?>
    </div>
    <div class="media-body">
        <?= \Yii::t('app', $info['text']) ?>
    </div>
</li>
