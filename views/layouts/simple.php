<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;

?>

<?php $this->beginContent('@app/views/layouts/simple-within-heafoo.php') ?>

<div class="container">
    <?= $content ?>
</div>

<?php $this->endContent() ?>

