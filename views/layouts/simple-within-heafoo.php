<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;

/** @var \yii\web\Controller $controller */
$controller = $this->context;
?>

<?php $this->beginContent('@app/views/layouts/blank-area.php'); ?>

    <div class="wrapper">
        <div class="wrapper-row">
            <div class="wrap">

                <?= $this->render('components-simple/menu-list.php', [
                    'navbar_id'   => 'custom-header-menu',
                    'controller'  => $controller,
                    'menu'        => $controller->menu,
                    'activeItems' => true,
                ]); ?>

                <?= $content ?>
            </div>
        </div>

        <footer class="footer">

            <?= $this->render('components-simple/menu-list.php', [
                'navbar_id'   => 'custom-footer-menu',
                'controller'  => $controller,
                'menu'        => $controller->menu,
                'activeItems' => false,
            ]); ?>

            <div class="container-fluid keep-end">
                <div class="container law-protection">
                    <div class="row">
                        <div class="col-sm-5">
                            <p class="right-protected">&copy; <?= \Yii::t('app', 'Transparent Diamonds. All rights reserved.') ?></p>
                        </div>
                        <div class="col-sm-7 policy-links">
                            <p>
                                <?= Html::a(\Yii::t('app', 'Terms And Conditions'), ['/terms/index'], ['class' => 'pull-right']); ?>
                                <?= Html::a(\Yii::t('app', 'Privacy Policy'), ['/terms/policy'], ['class' => 'pull-right']); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
<?php $this->endContent() ?>

