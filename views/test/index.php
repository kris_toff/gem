<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 17:57
 */
use kartik\tabs\TabsX;
use yii\helpers\Url;
?>

<?= TabsX::widget([
    'containerOptions' => [
        'data-enable-cache' => true,
    ],
    'items' => [
        [
            'label' => 'First',
            'content' => 'cccc',
            'active' => true,
            'linkOptions' => [
                'data-url' => Url::to(['update']),
            ]
        ],
        [
            'label' => 'Second',
            'content' => 'fsdf',
            'linkOptions' => [
                'data-url' => Url::to(['update']),
            ]
        ]
    ]
]) ?>
