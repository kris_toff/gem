<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 2:10
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \app\models\AR\NewsCategory $model */
?>

<div class="col-sm-6 col-md-3 news-col">
    <h4 class="simple-block-header"><strong><?= \Yii::t('app', $model->name) ?></strong></h4>

    <?php foreach ($model->getNews(4)->each(5) as $news): ?>
        <div>
            <a href="<?= Url::toRoute(['/news/story', 'id' => $news->id]) ?>" class="news-link"><?= \Yii::t('hill', $news->title) ?></a>
            <div class="news-date"><?= strtoupper(date('d M Y', $news->created_at)) ?></div>
        </div>
    <?php endforeach; ?>

    <div class="news-view-more"><?= Html::a(\Yii::t('app', 'View all'), ['/news/list', 'tab' => $tab, 'category'=>$model->id], ['class' => 'btn btn-link']) ?></div>
</div>
