<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.09.2016
 * Time: 12:21
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\components\wizard\topic\TopicWidget;
use app\components\widgets\customCarousel\CustomCarouselWidget;

/** @var \yii\web\View $this */
/** @var array $slItems */
/** @var integer $happiest_user */

?>

    <div class="bg-image">
        <div class="row" style="padding: 15px 0;">
            <div class="col-md-12">

                <?= CustomCarouselWidget::widget([
                    'json'         => true,
                    'items'        => $slItems,
                    /*/
                    'items'        => [
                        [
                            'href'    => Url::to(['/img/GRAPHIC.png']),
                            'options' => [
                                'data-bgtext' => 'testtest- test',
                                'data-smtext' => 'testtest- test',
                            ],
                        ],
                        [
                            'href'    => Url::to(['/img/logo.png']),
                            'options' => [
                            ],
                        ],
                    ],
                    /**/
                'showControls' => true,
                    'clientEvents' => [
                        'onslide' => '
function(index, slide) {
    var elem = $(this.container);
    var options = this.list[index]["options"];

    var big_text = options["data-bgtext"],
        small_text = options["data-smtext"],
        url = options["data-url"];
    var node = elem.find(".content");
    
    node.empty();
    var wrap = $("<div/>", {class: "wrap"});
    
    $("<div/>", {text: big_text, class: "container-header"}).appendTo(wrap);
    $("<div/>", {text: small_text, class: "container-text"}).appendTo(wrap);
    
    if (url != undefined && url.length > 0) $("<a/>", {text: "Get started", class: "btn btn-primary", href: url, target: "_blank"}).appendTo(wrap);
    node.append(wrap);
}',
                    ],
                ]) ?>

            </div>
        </div>

        <div class="row happiest-users table-wrap">
            <div class="table-row">
                <div class="col-md-4 col-md-offset-1 table-cell">
                    <div class="center-position">
                        <div class="center-subject">
                            <img src="/img/Pie-mini.png" class="img-responsive"/>
                            <div class="wrap-text text-center">
                                <div class="bigger-font"><strong><?= $happiest_user ?></strong></div>
                                <div class="smaller-font"><?= \Yii::t('app', 'Users') ?></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-7 table-cell" style="padding: 5% 15px;">
                    <div class="wrap-position">
                        <h4><strong>Full transparency on market</strong></h4>
                        <p>Transparent Diamond is at the cutting edge of price transparency for the diamond industry,
                            revealing
                            the most realistic and efficient diamond prices within the marketplace.</p>
                        <p>This platform is available to all types of users from non professional customers, jewelers to
                            traders
                            and investors.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <?= $this->render('/diamond-software/index', [
            'labels'      => $labels,
            'values'      => $values,
            'title'       => $title,
            'dcutmap'     => $dcutmap,
            'dcaratmap'   => $dcaratmap,
            'dclaritymap' => $dclaritymap,
            'dcolormap'   => $dcolormap,
            'dcutBtn'     => $dcutBtn,
        ]); ?>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= TopicWidget::widget([
                'title' => 'Recent News',
            ]) ?>

            <?php foreach ($news as $newItem): ?>

                <?= $this->render('news/_recent_news', [
                    'model' => $newItem,
                    'tab'   => $news_tab,
                ]) ?>

            <?php endforeach; ?>
        </div>
    </div>

<?php
$this->registerCss(<<<CSS
.happiest-users{
}
body > .wrapper{
background-clip:border-box;
background-color:inherit;
background-origin:border-box;
background-position: left 53%;
background-repeat:no-repeat;
background-size:100% 40%;
background-attachment:scroll;
background-image:url('/img/Shape_2.png'),url('/img/Shape_1.png');
background-blend-mode: normal;
}
.happiest-users img{
/*transform: rotate(15deg);*/
}
.center-position{
position: relative;
height: 270px;
}
.center-position .center-subject{
position:absolute;
right: 20%;
color: #0996cf;
}
.center-position .center-subject .wrap-text{
position:absolute;
top:31%;
width: 100%;
}
.center-subject .bigger-font{
font-size: 52px;
}
.center-subject .smaller-font{
font-size: 18px;
}
CSS
);