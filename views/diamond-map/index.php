<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 11:29
 */

use dosamigos\google\maps\Map;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\overlays\InfoWindow;
use app\components\wizard\topic\TopicWidget;
use yii\helpers\Json;
use yii\bootstrap\Html;

/** @var array $points */
?>

<?= TopicWidget::widget([
    'title' => 'The diamond map',
]) ?>

<div class="row">
    <p class="text-muted text-center"><?= \Yii::t('app', 'Select the location to gain an overview of the diamond process') ?></p>
</div>

<?php

$coord = new LatLng(['lat' => 27, 'lng' => 16]);
$map = new Map([
    'center'                 => $coord,
    'zoom'                   => 2,
    'styles'                 => Json::decode(require(__DIR__ . '/_mapStyle.inc.php')),
    'backgroundColor'        => 'transparent',
    'disableDoubleClickZoom' => false,
    'disableDefaultUI'       => false,
    'draggableCursor'        => 'resize',
    'containerOptions'       => [
        'id' => 'diamond-map-canvas',
    ],
]);

// Lets add a marker now
foreach ($points as $point) {
    $pointCC = new LatLng(['lat' => $point['latitude'], 'lng' => $point['longitude']]);
    $marker = new Marker([
        'position' => $pointCC,
        'title'    => $point['title'],
    ]);

    $marker->attachInfoWindow(new InfoWindow([
        'content' => "<p>{$point['text']}</p>",
    ]));
    // Add marker to the map
    $map->addOverlay($marker);
}

// Display the map -finally :)
echo Html::tag('div', $map->display(), ['class' => '', 'style'=>'margin-bottom: 20px;']);
?>