<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 18:00
 */

use app\components\wizard\topic\TopicWidget;
use kartik\tabs\TabsX;

/** @var array $tabs */
?>

<?= TopicWidget::widget([
    'title' => 'Recent news',
]) ?>

<div class="row">
    <?= TabsX::widget([
        'id'               => 'news-switch-tabs',
        'position'         => TabsX::POS_ABOVE,
        'align'            => TabsX::ALIGN_LEFT,
        'encodeLabels'     => true,
        'containerOptions' => [
            'class'             => 'col-sm-12 tabs-x',
            'data-enable-cache' => false,
        ],
        'options'          => [
            'class' => 'col-md-8 col-xs-12',
        ],
        'pluginEvents'     => [
            'tabsX.beforeSend' => 'function(event, jqXHR, settings){
                var tab = $(event.target);
                var container = $(event.currentTarget);
                container.find("ul.nav a[data-close]").each(function(index, element){
                    var link = $(element);
                    var cid = link.attr("href");
                    
                    $(cid).remove();
                    link.parent("li").remove();
                });
            }',
        ],
        'items'            => $tabs,
    ]); ?>
</div>

