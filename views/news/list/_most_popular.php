<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:11
 */

use yii\bootstrap\Html;

/** @var \yii\data\ActiveDataProvider $popularDataProvider */
?>

<div class="simple-block">
    <div class="simple-block-header"><strong><?= \Yii::t('app', 'Most popular') ?></strong></div>

    <ul class="media-list">
        <?php foreach ($popularDataProvider->getModels() as $model): ?>

            <li class="media">
                <span class="news-date pull-left"><?= $model->created ?></span>
                <div class="media-body">
                    <?= Html::a(Html::encode(\Yii::t('hill', $model->title)), ['story' , 'id' => $model->id]) ?>
                </div>
            </li>

        <?php endforeach; ?>
    </ul>
</div>
