<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 13:20
 */

/** @var \app\models\AR\News $model */

?>
<?php
$title = \Yii::t('hill', $model->title);
?>

<div class="media">
    <div class="media-img-object pull-left">
        <img class="media-object img-responsive" src="<?= $model->main_image ?>" alt="<?= $title ?>"/>
    </div>

    <div class="media-body">
        <h4 class="media-heading"><strong><?= $title ?></strong></h4>
        <div class="media-date"><?= $model->created ?></div>
        <?= \Yii::t('hill', $model->preview) ?>
    </div>
</div>
