<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 13:17
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\wizard\searchMini\SearchMiniWidget;
use app\components\wizard\categoriesUnit\CategoryWidget;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var integer $tab */
/** @var array $categories */
/** @var \yii\data\ActiveDataProvider $mostPopular */
/** @var \app\models\SearchModule $searchModel */
?>


<div class="col-sm-12 col-md-8 news-list">
    <div class="list-view">
        <?php print Html::beginTag('div', ['class' => 'list-group']) ?>

        <?php foreach ($dataProvider->getModels() as $model): ?>

            <a href="<?= Url::to(['story', 'id' => $model->id]) ?>" class="list-group-item">
                <?= $this->render('_item_list', ['model' => $model]); ?>
            </a>

        <?php endforeach; ?>

        <?php print LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ]) ?>

        <?php print Html::endTag('div') ?>
    </div>
    <? /*/= ListView::widget([
        'id'           => 'list-news-unit',
        'dataProvider' => $dataProvider,
        'itemView'     => '_item_list',
        'itemOptions'  => [
            'class' => 'list-group-item',
            'tag' => 'a',
        ],
        'options' => [
            'class' => 'list-group',
            'tag' => 'div',
        ],
        'pager' => [

        ],
        'summary'      => '',
    ]) /**/ ?>
</div>
<div class="col-md-4 col-sm-12">

    <?= SearchMiniWidget::widget([
        'model' => $searchModel,
        'title' => 'News search',
        'url'   => Url::to(['news/list']),
    ]) ?>

    <?= CategoryWidget::widget([
        'items' => $categories,
    ]) ?>

    <?= $this->render('_most_popular', [
        'popularDataProvider' => $mostPopular,
    ]) ?>
</div>

<div class="clearfix"></div>

