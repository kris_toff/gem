<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 21:33
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \app\models\AR\News[] $models */
?>

<div class="simple-block-header"><strong><?= count($models) ? \Yii::t('app', 'Related news') :'' ?></strong></div>

<ul class="media-list news-related">
    <?php foreach($models as $model): ?>

    <?php
    $linkNews = Url::to(['story', 'id' => $model->id]);

        $title = Html::encode(\Yii::t('hill', $model->title));
    ?>
        <li class="media">
            <a class="pull-left" href="<?= $linkNews ?>">
                <img class="img-responsive media-object" src="<?= $model->main_image ?>" alt="<?= $title ?>">
            </a>
            <div class="media-body">
                <a href="<?= $linkNews ?>"><h4 class="media-heading"><?= $title ?></h4></a>
                <?= Html::tag('div', $model->created, ['class' => 'news-date']) ?>
            </div>
        </li>

    <?php endforeach; ?>
</ul>
