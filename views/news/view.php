<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 20:59
 */

use app\components\wizard\searchMini\SearchMiniWidget;
use app\components\wizard\categoriesUnit\CategoryWidget;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \app\models\AR\News $model */
/** @var array $categories */
/** @var \app\models\AR\News[] $related */
?>

<?php
$title = \Yii::t('hill', $model->title);
?>

<div class="row">
    <div class="col-md-8">
        <h3 class="news-heading"><strong><?= Html::encode($title) ?></strong></h3>
        <div class="news-date"><?= $model->created ?></div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">

        <?php if (!empty($model->main_image)): ?>
        <div class="news-img">
            <img class="img-responsive" alt="<?= Html::encode($title) ?>" title="<?= Html::encode($title) ?>" src="<?= $model->main_image ?>"/>
        </div>
        <?php endif; ?>

        <div class="news-body"><?= \Yii::t('hill', $model->content) ?></div>
        <hr/>
        <?= $this->render('news/_related_news',[
            'models' => $related,
        ]) ?>
    </div>
    <div class="col-md-4">
        <?= SearchMiniWidget::widget() ?>

        <?= CategoryWidget::widget([
            'items' => $categories,
        ]) ?>
    </div>
</div>