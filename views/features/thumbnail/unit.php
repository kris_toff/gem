<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.10.2016
 * Time: 13:26
 */

/** @var string $label */
/** @var string $caption */
/** @var string $src */
?>

<?php
$label = \Yii::t('app', $label);
?>

<div class="col-md-3 col-sm-6">
    <div class="thumbnail">
        <div class="thumbnail-icon"><img src="<?= $src ?>" height="40" alt="<?= $label ?>"/></div>
        <div class="caption">
            <h4 class="thumbnail-label"><?= $label ?></h4>
            <p><?= \Yii::t('app', $caption) ?></p>
        </div>
    </div>
</div>
