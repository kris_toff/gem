<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.09.2016
 * Time: 10:26
 */
use yii\bootstrap\Alert;
use app\components\wizard\topic\TopicWidget;

?>

<?php
print TopicWidget::widget([
    'title' => 'Features we offer',
])
?>

<div class="row">
    <div id="features-thumbnail">
        <?= $this->render('thumbnail/unit', [
            'label'   => 'Verification',
            'caption' => 'This website provides an imperative verification tool for customers,',
            'src'     => '/img/user.png',
        ]); ?>

        <?= $this->render('thumbnail/unit', [
            'label'   => 'Software',
            'caption' => 'This website provides an imperative verification tool for customers,',
            'src'     => '/img/computer.png',
        ]); ?>

        <?= $this->render('thumbnail/unit', [
            'label'   => 'Conflict Free',
            'caption' => 'This website provides an imperative verification tool for customers,',
            'src'     => '/img/handshake.png',
        ]); ?>

        <?= $this->render('thumbnail/unit', [
            'label'   => 'Transparency',
            'caption' => 'This website provides an imperative verification tool for customers,',
            'src'     => '/img/bar-chart.png',
        ]); ?>

    </div>
</div>
<div class="clearfix"></div>