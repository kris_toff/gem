<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 12:18
 */

/** @var \yii\data\ActiveDataProvider $dataProvider */
use dosamigos\chartjs\ChartJs;
?>

<?= ChartJs::widget([
    'type' => 'line',
    'options'       => [
        'id' => 'diamondChartGraph',
        'height' => 140,
        'width' => 620,
    ],
    'clientOptions' => [
        'scales' => [
            'yAxes' => [
                [
                    'ticks' => [
                        'beginAtZero' => true,
                    ],
                    'gridLines' => [
                        'display' => false,
                    ]
                ],
                [
                    'stacked' => true,
                ]
            ],
            'xAxes' => [
                [
                    'stacked' => true,

                ]
            ]
        ],
        'legend' => [
            'display' => false,
            'labels' => [
                'fontColor' => 'rgb(255, 99, 132)',
            ]
        ]
    ],
    'data'          => [
        'labels'   => ['1/16', '2/16', '3/16', '4/16', '5/16', '6/16', '7/16', '8/16', '9/16', '10/16', '11/16', '12/16'],
        'datasets' => [
            [
                'fillColor'        => "rgba(220,220,220,0.5)",
                'strokeColor'      => "rgba(220,220,220,1)",
                'pointColor'       => "rgba(220,220,220,1)",
                'pointStrokeColor' => "#fff",
                'data'             => [65, 59, 100, 150],
            ],
        ],
    ],
]);