<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 18:08
 */

use yii\bootstrap\Html;

/** @var \app\models\AR\Education $model */
/** @var array $menu */

?>

<div class="row">
    <div class="col-md-8 col-md-offset-4">
        <h3 class="text-center"><strong><?= Html::encode(\Yii::t('hill', $model->name)) ?></strong></h3>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-md-push-3 news-content">

        <div class="pull-right news-img"><?= Html::img($model->preview_image, ['class' => 'img-responsive img-thumbnail']) ?></div>

        <?= $model->content ?>

        <div class="clearfix"></div>
    </div>

    <div class="col-md-3 col-md-pull-9">
        <?= $this->render('_menu', ['items' => $menu]) ?>
    </div>

</div>
<div class="clearfix"></div>