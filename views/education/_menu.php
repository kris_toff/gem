<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 10:49
 */

use yii\bootstrap\Collapse;
use kartik\sidenav\SideNav;
use yii\helpers\Url;

/** @var array $items */

?>

<div id="education-menu">
    <?= SideNav::widget([
        'items' => $items,
        'encodeLabels' => true,
        'indMenuClose' => '<i class="indicator glyphicon glyphicon-chevron-down"></i>',
        'indMenuOpen' => '<i class="glyphicon glyphicon-chevron-up"></i>',
//        'route' => Url::toRoute(['test']),
        /*/
        'items' => [
            [
                'url' => '/home',
                'label' => 'Home',
//                'icon' => 'home'
            ],
            [
                'label' => 'Help',
                'icon' => 'question-sign',
                'items' => [
                    ['label' => 'About', 'icon'=>'info-sign', 'url'=>'#'],
                    ['label' => 'Contact', 'icon'=>'phone', 'url'=>'#'],
                ],
            ],
        ],
        /**/
    ])?>
</div>
