<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 2:33
 */

use app\components\wizard\topic\TopicWidget;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array $menu */
/** @var \app\models\AR\EducationMenu|null $model */
/** @var \app\models\AR\Education[] $lessons */
?>

<?= TopicWidget::widget([
    'title' => 'Diamond education',
]) ?>

<div class="row">
    <div class="col-sm-4 col-md-3">
        <?= $this->render('_menu', ['items' => $menu]) ?>
    </div>
    <div class="col-sm-8 col-md-9">
        <?php if (!is_null($model)): ?>
            <div class="row education-header">
                <div class="col-sm-12">
                    <?= \Yii::t('hill', $model->header) ?>
                </div>
                <div class="clearfix"></div>
            </div>

            <?php
            $chunk = array_chunk($lessons, 3);
            $part1 = array_filter(ArrayHelper::getColumn($chunk, 0, []));
            $part2 = array_filter(ArrayHelper::getColumn($chunk, 1, []));
            $part3 = array_filter(ArrayHelper::getColumn($chunk, 2, []));
            ?>

            <div class="row">

                <div class="col-md-4">
                    <?php foreach ($part1 as $lesson) {
                        print $this->render('_item_edu', ['model' => $lesson]);
                    } ?>
                </div>
                <div class="col-md-4">
                    <?php foreach ($part2 as $lesson) {
                        print $this->render('_item_edu', ['model' => $lesson]);
                    } ?>
                </div>
                <div class="col-md-4">
                    <?php foreach ($part3 as $lesson) {
                        print $this->render('_item_edu', ['model' => $lesson]);
                    } ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
