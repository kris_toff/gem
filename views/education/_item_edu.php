<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 15:07
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\models\AR\Education $model */

?>

<div class="lesson-unit">
    <div class="thumbnail">
        <div class="lesson-unit-img"><a href="<?= Url::toRoute([
                'lesson',
                'id' => $model->id,
            ]) ?>"><?= Html::img($model->preview_image, ['class' => 'img-responsive']) ?></a></div>
        <div class="caption">
            <h4><strong><?= Html::encode(\Yii::t('hill', $model->name)) ?></strong></h4>
            <p><?= \Yii::t('hill', $model->preview_text) ?></p>

            <?php if (!empty($model->content)): ?>
                <p class="lesson-more"><?= Html::a(\Yii::t('app', 'Read more'), [
                        'lesson',
                        'id' => $model->id,
                    ], ['class' => 'btn btn-link']) ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
