<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 23:44
 */

use kartik\tabs\TabsX;
use app\components\wizard\topic\TopicWidget;

/** @var \yii\web\View $this */
/** @var array $tabs */
?>

<?= TopicWidget::widget([
    'title' => 'Our video',
]) ?>

<div class="row">
    <?= TabsX::widget([
        'id'               => 'video-switch-tabs',
        'position'         => TabsX::POS_ABOVE,
        'align'            => TabsX::ALIGN_LEFT,
        'encodeLabels'     => false,
        'containerOptions' => [
            'class'             => 'col-sm-12 tabs-x',
            'data-enable-cache' => false,
        ],
        'options'          => [
            'class' => 'col-md-8 col-xs-12',
        ],
        'pluginEvents'     => [
            'tabsX.beforeSend' => 'function(event){
                var tab = $(event.target);
                var container = $(event.currentTarget);
                var iframes = $("iframe");
                iframes.each(function(index, element){
                    var elem = $(element);
                    elem.attr("src", elem.attr("src"));
                });
                
                container.find("ul.nav a[data-close]").each(function(index, element){
                    var link = $(element);
                    var cid = link.attr("href");
                    
                    $(cid).remove();
                    link.parent("li").remove();
                });
            }',
        ],
        'items'            => $tabs,
    ]); ?>

</div>
