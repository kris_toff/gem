<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.09.2016
 * Time: 12:11
 */

use yii\widgets\ListView;
use app\components\wizard\searchMini\SearchMiniWidget;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var integer $tab */
/** @var \yii\web\View $this */
?>

<div class="col-sm-12 col-md-8 video-list">
    <?= ListView::widget([
        'id'           => 'list-video-unit',
        'dataProvider' => $dataProvider,
        'itemView'     => '_item_list',
        'itemOptions'  => [
            'class' => 'row',
        ],
        'pager' => [

        ],
        'summary'      => '',
    ]) ?>
</div>

<div class="col-md-4 hidden-sm">
    <?= SearchMiniWidget::widget([
        'title' => 'Videos search',
        'model' => $searchModel,
        'url' => Url::toRoute(['video/list']),
    ]) ?>
</div>

<div class="clearfix"></div>
