<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.09.2016
 * Time: 13:52
 */

/** @var \app\models\AR\Video $model */
?>

<div class="video-unit">
    <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://www.youtube.com/embed/<?= $model->link ?>?rel=0&amp;showinfo=0" allowfullscreen></iframe>
    </div>
    <div class="video-label"><strong><?= $model->label ?></strong></div>
    <div class="video-created"><?= $model->created ?></div>
</div>
