<?php

namespace app\models;

use app\models\AR\PhoneCode;
use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends ActiveRecord {
    public $subject;
    public $verifyCode;

    public static function tableName() {
        return '{{feedback}}';
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }


    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // name, email, phone and body are required
            [['name', 'email', 'phone', 'body'], 'filter', 'filter' => 'trim'],
            [['name', 'email', 'phone', 'body'], 'required'],
            // phone must contains only digits after trim spaces
            ['phone', 'app\handler\validators\PhoneNumberValidator'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'skipOnEmpty' => true,],
            // message has min 15 symbols
            ['body', 'string', 'min' => 15],
            ['name', 'string', 'min' => 2],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'name'       => \Yii::t('app', 'Name'),
            'email'      => \Yii::t('app', 'E-mail'),
            'phone'      => \Yii::t('app', 'Phone'),
            'body'       => \Yii::t('app', 'Message'),
            'verifyCode' => \Yii::t('app', 'Verification Code'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     *
     * @return boolean whether the model passes validation
     */
    public function contact($email) {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }

        return false;
    }

}
