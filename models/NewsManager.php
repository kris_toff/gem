<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.10.2016
 * Time: 16:56
 */

namespace app\models;


use app\models\AR\News;
use yii\base\Model;
use app\models\AR\NewsTabs;
use app\models\AR\NewsCategory;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

class NewsManager extends Model {
    public static function random() {
        $tabNews = NewsTabs::find()->where(['active' => 1])->with([
            'categories',
        ])->limit(1)->one();

        if ($tabNews == null) {
            $tabNews = NewsTabs::find()->select('id')->column();

            if (!count($tabNews))
                return [];

            $tabN = $tabNews[ array_rand($tabNews) ]->id;
            $newsC = NewsCategory::find()->where(['tab_id' => $tabN])->all();
        } else {
            $tabN = $tabNews->id;
            $newsC = $tabNews->categories;

            $ids = count($newsC) ? (array)array_rand($newsC, min(4, count($newsC))) : [];
            $newsC = array_intersect_key($newsC, array_flip($ids));
        }

        return count($newsC) ? [
            'tab' => $tabN,
            'list' => $newsC,
        ] : [];
    }
}