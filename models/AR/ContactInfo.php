<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 12:00
 */

namespace app\models\AR;


use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ContactInfo extends ActiveRecord{
    public $image;

    public function init() {
        parent::init();

        $this->active = 1;
    }

    public function attributeLabels() {
        return [
            'image' => \Yii::t('app', 'Icon'),
            'icon' => \Yii::t('app', 'Icon'),
            'text' => \Yii::t('app', 'Text'),
            'active' => \Yii::t('app', 'Active?'),
        ];
    }

    public function rules(){
        return [

            ['text', 'required'],
            [['icon', 'text'], 'string'],
            ['image', 'image'],
            ['active', 'boolean'],
            ['active', 'default' , 'value' => 1],
        ];
    }

    public function beforeSave($insert) {
        $file = UploadedFile::getInstance($this, 'image');

        if ($file != null){
            $path = \Yii::getAlias('@webroot');
            $filePath = '/img/contacts';

            FileHelper::createDirectory($path . $filePath);

            do{
                $fileName = \Yii::$app->getSecurity()->generateRandomString(12) . '.'. $file->getExtension();
                $filePath = '/img/contacts/' . $fileName;
            }while(file_exists($path . $filePath));

            $file->saveAs($path . $filePath);
            $this->icon = $filePath;
        }

        return parent::beforeSave($insert);
    }
}