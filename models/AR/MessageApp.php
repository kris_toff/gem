<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 16:25
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class MessageApp extends ActiveRecord{
    public function rules(){
        return [
            [['language', 'translation'], 'trim'],
            [['id', 'language', 'translation'], 'required'],
            [['language', 'translation'], 'string'],
            ['id', 'integer', 'min' => 1],
        ];
    }
}