<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 10:43
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class EducationMenu extends ActiveRecord{

    public function getLessons(){
        return $this->hasMany(Education::className(), ['menu_id' => 'id']);
    }

    public function attributeLabels() {
        return [
            'label' => \Yii::t('app', 'Name'),
            'header' => \Yii::t('app', 'Top text category'),
        ];
    }

    public function rules() {
        return [
            [['label', 'header'] , 'string'],
        ];
    }

}