<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 19:00
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class NewsTabs extends ActiveRecord{
    public function rules(){
        return [
            [['label', 'active'], 'required'],
            ['active', 'boolean'],
            ['label', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'label' => \Yii::t('app', 'Name'),
            'active' => \Yii::t('app', 'Active'),
        ];
    }

    public function getCategories($count = 0){
        return $this->hasMany(NewsCategory::className(), ['tab_id' => 'id']);
    }
}