<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.09.2016
 * Time: 13:55
 */

namespace app\models\AR;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class Video extends ActiveRecord {
    public function rules() {
        return [
            ['link', 'filter', 'filter' => [$this, 'extractId'], 'skipOnArray' => true],
            ['link', 'match', 'pattern' => '/^[a-z0-9]{3,18}$/i',],
            [['tab_id', 'label', 'link'], 'required'],
            [['tab_id'], 'integer', 'min' => 1],
            [['name', 'label', 'link',], 'string'],

        ];
    }

    public function behaviors() {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => \Yii::t('app', 'Name video'),
            'label' => \Yii::t('app', 'Title video'),
            'link' => \Yii::t('app', 'Link at YouTube'),
            'tab_id' => \Yii::t('app', 'Video tab'),
            'created_at' => \Yii::t('app', 'Created date'),
            'updated_at' => \Yii::t('app', 'Updated date'),
        ];
    }

    public function extractId( $value ) {
        $value = urldecode($value);
        $code = '[a-zA-Z0-9]{3,18}';
        $pattern = "watch\\?v=($code)";

        preg_match("!$pattern!i", $value, $matches);
        $rsl = ArrayHelper::getValue($matches, 1);

        return is_null($rsl) ? $value : $rsl;
    }

    public function getTab() {
        return $this->hasOne(VideoTabs::className(), ['id' => 'tab_id']);
    }

    public function getCreated() {
        return date('H:i d-M-Y', $this->created_at);
    }

    public function linkYoutube($text, $options = []) {
        return Html::a($text, "https://www.youtube.com/watch?v={$this->link}", $options);
    }
}
