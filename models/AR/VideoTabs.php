<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.09.2016
 * Time: 10:20
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class VideoTabs extends ActiveRecord{

    public function attributeLabels() {
        return [
            'label' => \Yii::t('app', 'Name'),
            'aactive' => \Yii::t('app', 'Active'),
        ];
    }

    public function rules(){
        return [
            ['label' , 'required'],
            ['active', 'default' , 'value' => 0],
        ];
    }

    public function getVideos(){
        return $this->hasMany(Video::className(), ['tab_id' => 'id']);
    }
}