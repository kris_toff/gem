<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.09.2016
 * Time: 17:02
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class PhoneCode extends ActiveRecord{
    public static function tableName() {
        return '{{phone_country_code}}';
    }

}