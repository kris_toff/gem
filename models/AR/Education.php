<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 10:42
 */

namespace app\models\AR;


use app\components\behaviors\TranslateBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Education extends ActiveRecord {
    public $main_img;
    public $translations;

    public function attributeLabels() {
        return [
            'menu_id'       => \Yii::t('app', 'Category lesson'),
            'name'          => \Yii::t('app', 'Name'),
            'preview_text'  => \Yii::t('app', 'Preview in thumbnail'),
            'preview_image' => \Yii::t('app', 'Preview image'),
            'created_at'    => \Yii::t('app', 'Created date'),
        ];
    }

    public function rules() {
        return [
            [['menu_id', 'name', 'preview_text', 'content', 'author_id'], 'required'],
            [['menu_id', 'author_id'], 'integer'],
            [['name', 'preview_text', 'preview_image', 'content'], 'string'],
            ['main_img', 'image',],
            ['translations', 'safe'],
        ];
    }

    public function getCategory() {
        return $this->hasOne(EducationMenu::className(), ['id' => 'menu_id']);
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class' => TranslateBehavior::className(),
                'fields' => ['name', 'preview_text', 'content'],
            ]
        ];
    }

    public function beforeValidate() {
        $this->author_id = \Yii::$app->user->id;

        return parent::beforeValidate();
    }

    public function beforeSave($insert) {
        $filePath = '/img/educations';
        $path = \Yii::getAlias('@webroot' . $filePath);

        $file = UploadedFile::getInstance($this, 'main_img');
        if ($file != null && FileHelper::createDirectory($path)) {
            do {
                $fileName = \Yii::$app->getSecurity()->generateRandomString(12) . '.' . $file->getExtension();
                $filePath = '/img/educations/' . $fileName;
            } while (file_exists($path . '/' . $fileName));

            $file->saveAs($path . '/' . $fileName);

            $this->preview_image = $filePath;
        }

        return parent::beforeSave($insert);
    }
}