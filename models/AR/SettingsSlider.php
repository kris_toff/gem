<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 16:33
 */

namespace app\models\AR;


use app\components\behaviors\TranslateBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class SettingsSlider extends ActiveRecord{
    public $image;
    public $translations;

    public function behaviors() {
        return [
            [
                'class' => TranslateBehavior::className(),
                'fields' => ['small_text', 'big_text', ],
            ],
        ];
    }

    public function rules(){
        return [
            [['src', 'big_text', 'small_text', 'url'], 'string'],
            ['image', 'image'],
            ['translations' , 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'src' => \Yii::t('app', 'Image'),
            'small_text' => \Yii::t('app', 'Text in small print'),
            'big_text' => \Yii::t('app', 'Header text'),
            'url' => \Yii::t('app', 'Jump when click'),
            'image' => \Yii::t('app', 'Image'),
        ];
    }

    public function beforeSave($insert) {
        $file = UploadedFile::getInstance($this, 'image');
        if ($file != null){
            $path = \Yii::getAlias('@webroot');
            FileHelper::createDirectory($path . '/img/slider');

            do{
                $fileName = \Yii::$app->getSecurity()->generateRandomString(12) . '.' .$file->getExtension();
                $filePath = '/img/slider/' . $fileName;
            }while(file_exists($path . $filePath));

            $file->saveAs($path . $filePath);

            $this->src = $filePath;
        }

        return parent::beforeSave($insert);
    }
}