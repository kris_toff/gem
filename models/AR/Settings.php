<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 15:01
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class Settings extends ActiveRecord{


    public function rules(){
        return [
            [['key', 'value'], 'string'],
        ];
    }
}