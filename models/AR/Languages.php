<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 12:30
 */

namespace app\models\AR;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Languages extends ActiveRecord{

    public function rules(){
        return [
            [['name', 'code',], 'trim'],
            [['name', 'code',], 'required'],

        ];
    }

    public static function getList(){
        $l = self::find()->select('id, name')->orderBy('id')->all();
        return ArrayHelper::map($l, 'id', 'name');
    }

    public static function getCodes(){
        $l = self::find()->select('id, code')->orderBy('id')->all();
        return ArrayHelper::map($l, 'id', 'code');
    }

}