<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 12:15
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

/**
 * @property integer $diamond_cut_rid
 * @property integer $diamond_carat_rid
 * @property integer $diamond_clarity_rid
 * @property integer $diamond_color_rid
 */
class DiamondPrice extends ActiveRecord {
    const SCENARIO_CALCULATE = 'calculate';

    public static function monthStr($num) {
        $dt = new \DateTime();
        $dt->setDate(date('Y'), $num, date('d'));

        return $dt->format('F');
    }

    public function attributeLabels() {
        return [
            'diamond_cut_rid'     => \Yii::t('app', 'Diamond cut'),
            'diamond_carat_rid'   => \Yii::t('app', 'Diamond carat'),
            'diamond_clarity_rid' => \Yii::t('app', 'Diamond clarity'),
            'diamond_color_rid'   => \Yii::t('app', 'Diamond color'),
        ];
    }

    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[ self::SCENARIO_CALCULATE ] = [
            'diamond_cut_rid',
            'diamond_carat_rid',
            'diamond_clarity_rid',
            'diamond_color_rid',
        ];

        return $scenarios;
    }

    public function rules() {
        return [
            [
                [
                    'diamond_cut_rid',
                    'diamond_carat_rid',
                    'diamond_clarity_rid',
                    'diamond_color_rid',
                    'date',
                ],
                'integer',
                'min' => 1,
            ],
            [
                'cost',
                'double',
            ],
        ];
    }

    public function calculate() {
        $queryWhere = [
            'diamond_cut_rid'     => $this->diamond_cut_rid,
            'diamond_carat_rid'   => $this->diamond_carat_rid,
            'diamond_clarity_rid' => $this->diamond_clarity_rid,
            'diamond_color_rid'   => $this->diamond_color_rid,
        ];

        $dt = DiamondPrice::find()->where($queryWhere)->max('[[date]]');

        if (is_null($dt)) {
            return false;
        }

        $queryWhere['date'] = $dt;
        $dl = (new \DateTime())->setTimestamp($dt);

        return [
            'price'      => \Yii::$app->getFormatter()->asDecimal(self::find()->select('cost')->where($queryWhere)->limit(1)->scalar(), 2) ?: 'none',
            'timestamp' => $dt,
            'date'      => $dl->format('d F Y'),
        ];
    }

    public function getCut() {
        return $this->hasOne(DiamondCut::className(), ['id' => 'diamond_cut_rid']);
    }

    public function getCarat() {
        return $this->hasOne(DiamondCarat::className(), ['id' => 'diamond_carat_rid']);
    }

    public function getClarity() {
        return $this->hasOne(DiamondClarity::className(), ['id' => 'diamond_clarity_rid']);
    }

    public function getColor() {
        return $this->hasOne(DiamondColor::className(), ['id' => 'diamond_color_rid']);
    }
}