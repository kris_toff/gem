<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 16:14
 */

namespace app\models\AR;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class MapPoints extends ActiveRecord{
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }


    public function rules(){
        return [
            [['name', 'tooltip', 'text', 'latitude', 'longitude', ], 'required'],
            [['name', 'tooltip', 'title', 'text',], 'string'],
            [['latitude', 'longitude'], 'double', ],
            ['active', 'default', 'value' => 1],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => \Yii::t('app', 'Name of point'),
            'tooltip' => \Yii::t('app', 'Hover tooltip'),
            'title' => \Yii::t('app', 'Description in the header'),
            'text' => \Yii::t('app', 'Description of point'),
            'latitude' => \Yii::t('app', 'Latitude'),
            'longitude' => \Yii::t('app', 'Longitude'),
            'created_at' => \Yii::t('app', 'Created date'),
            'updated_at' => \Yii::t('app', 'Updated date'),
        ];
    }
}