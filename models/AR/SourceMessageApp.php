<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 16:20
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class SourceMessageApp extends ActiveRecord{
    public function rules(){
        return [
            ['category', 'default', 'value' => 'app'],
            [['category', 'message'], 'required'],
            [['category', 'message'], 'string'],
            [['category', 'message'], 'unique', 'targetAttribute' => ['category', 'message']],
        ];
    }
}