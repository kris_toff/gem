<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 23:13
 */
namespace app\models\AR;

use app\components\behaviors\TranslateBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class News extends ActiveRecord {
    public $preview_image;
    public $translations;

    public function rules() {
        return [
            ['category_id', 'integer'],
            [['name', 'title', 'preview', 'content',], 'string'],
            [['category_id', 'title', 'preview', 'content'], 'required'],
            ['preview_image', 'image'],
            ['translations', 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'category_id' => \Yii::t('app', 'Category'),
            'main_image'  => \Yii::t('app', 'Preview image'),
            'preview'     => \Yii::t('app', 'Announce'),
            'content'     => \Yii::t('app', 'Content'),
            'created_at'  => \Yii::t('app', 'Created date'),
            'updated_at'  => \Yii::t('app', 'Updated date'),
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class' => TranslateBehavior::className(),
                'fields' => ['title', 'preview', 'content'],
            ]
        ];
    }

    public function beforeSave($insert) {
        $this->author_id = \Yii::$app->user->id;

        $file = UploadedFile::getInstance($this, 'preview_image');
        if ($file != null) {
            $path = \Yii::getAlias('@webroot/img/news');

            do {
                $fileName = \Yii::$app->getSecurity()->generateRandomString(12) . '.' . $file->getExtension();
                $filePath = $path . '/' . $fileName;
            } while (file_exists($filePath));

            FileHelper::createDirectory($path);
            $file->saveAs($filePath);

            $this->main_image = '/img/news/' . $fileName;
        }

        return parent::beforeSave($insert);
    }

    public function getCreated() {
        return date('H:i d-M-Y', $this->created_at);
    }

    public function getCategory() {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    public function getRelatedList() {
        return $this->hasMany(NewsRelated::className(), ['source_news_id' => 'id']);
    }
}