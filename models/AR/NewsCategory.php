<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 21:23
 */

namespace app\models\AR;


use yii\db\ActiveRecord;

class NewsCategory extends ActiveRecord {

    public function rules() {
        return [
            ['tab_id', 'integer',],
            ['name', 'required'],
            ['name', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'tab_id' => \Yii::t('app', 'Tab'),
            'name'   => \Yii::t('app', 'Name'),
        ];
    }

    public function getTab() {
        return $this->hasOne(NewsTabs::className(), ['id' => 'tab_id']);
    }

    public function getNews($count = null) {
        $query = $this->hasMany(News::className(), ['category_id' => 'id']);
        if (!is_null($count) && is_integer($count))
            $query->limit($count);
        $query->orderBy(['created_at' => SORT_DESC]);

        return $query;
    }
}