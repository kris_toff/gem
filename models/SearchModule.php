<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.09.2016
 * Time: 19:45
 */

namespace app\models;


use yii\base\Model;

class SearchModule extends Model{
    public $text;
//    public $tab;
//    public $category;

    public function rules() {
        return [
            ['text', 'required'],
            ['text', 'string', 'min' => 2, ],
//            [['tab', 'category',], 'integer', 'min' => 1],
        ];
    }
}