<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 21:45
 */

namespace app\models;


use app\models\AR\NewsRelated;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class RelatedNews extends Model {
    public $relatedList;
    protected $_sourceId;

    public function rules() {
        return [
            ['relatedList', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'relatedList' => \Yii::t('app', 'Related news'),
        ];
    }


    public function setSource($id) {
        $this->_sourceId = $id;

        $this->relatedList = empty($this->_sourceId) ? [] : implode(',' ,NewsRelated::find()
            ->select('related_news_id')
            ->where(['source_news_id' => $this->_sourceId])
            ->asArray()
            ->column());
    }

    public function save($valid = true){
        if ($valid && !$this->validate()){
            return false;
        }

        $rel = array_filter(explode(',', $this->relatedList));

        $currentRelated = NewsRelated::find()->where(['source_news_id' => $this->_sourceId])->indexBy('related_news_id')->asArray()->all();
        $newList = array_diff_key(array_flip($rel), $currentRelated);
        if (count($newList)>0){
            $ins = [];
            foreach($newList as $key => $item){
                $ins[] = [$this->_sourceId, $key];
            }
            \Yii::$app->db->createCommand()->batchInsert(NewsRelated::tableName(), [
                'source_news_id', 'related_news_id',
            ], $ins)->execute();
        }

        $delList = array_diff_key($currentRelated, array_flip($rel));
        if (count($delList)>0){
            NewsRelated::deleteAll(['id' => ArrayHelper::getColumn($delList, 'id')]);
        }

        return true;
    }
}
