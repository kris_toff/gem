<?php

namespace app\handler\validators;

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 0:02
 */
use yii\validators\Validator;
use app\models\AR\PhoneCode;
use yii\helpers\ArrayHelper;

class PhoneNumberValidator extends Validator {
    public function init() {
        parent::init();

        $this->message = \Yii::t('app', 'Invalid phone number.');
    }

    public function validateAttribute($model, $attribute) {
        $phones_code = PhoneCode::find()->all();
        $codes = ArrayHelper::getColumn($phones_code, 'code');
        $phone = $model->$attribute;

        // pattern: \+(?:code1|code2...)\d{6,12}
        $pattern = '^\\+(?:' . implode('|', $codes) . ')\\d{6,12}$';
        if (!preg_match("~$pattern~", preg_replace('~[\s()-]+~', '', $phone))) {
            // ошибка в номере
            $this->addError($model, $attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

// возвращаемый код будет вызван в виде функции, куда передаются параметры
//        attribute: имя атрибута для проверки.
//        value: проверяемое значение.
//        messages: массив, используемый для хранения сообщений об ошибках, проверки значения атрибута.
//        deferred: массив, который содержит отложенные объекты (описано в следующем подразделе).

        return <<<JS
value = value.replace(/[\s()-]+/g, '');
if (value.match(/^\+?\d{8,15}$/i) == null){
    messages.push({$message});
}
JS;
    }
}