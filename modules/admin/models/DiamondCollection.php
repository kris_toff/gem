<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.10.2016
 * Time: 13:56
 */

namespace app\modules\admin\models;


use app\models\AR\DiamondCarat;
use app\models\AR\DiamondClarity;
use app\models\AR\DiamondColor;
use app\models\AR\DiamondCut;
use app\models\AR\DiamondPrice;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use moonland\phpexcel\Excel;
use yii\helpers\VarDumper;

class DiamondCollection extends Model {
    public    $insertDate   = 0;
    public    $distribution = 0;
    public    $file         = null;
    protected $_filePath    = null;

    public function attributeLabels() {
        return [
//            'date' => \Yii::t('app', 'Date'),
            'file'         => \Yii::t('app', 'File'),
            'distribution' => \Yii::t('app', 'Distribution for price'),
        ];
    }

    public function rules() {
        return [
//            ['date', 'required'],
            [['file', 'distribution'], 'required'],
//            ['date', 'date', 'format' => 'php:d F Y'],
            ['file', 'file',],
            ['distribution', 'number', 'min' => 0, 'max' => 100],
        ];
    }

    public function parse() {
        $data = Excel::import($this->_filePath, [
            'setFirstRecordAsKeys' => false,
            'setFirstTitle'        => false,
        ]);

        $colCuts = ArrayHelper::getColumn($data, 'A');
        $colCarats = ArrayHelper::getColumn($data, 'B');
        $colClarity = ArrayHelper::getColumn($data, 'C');
        $colColors = ArrayHelper::getColumn($data, 'D');
        $colCosts = ArrayHelper::getColumn($data, 'E');

        $cuts = array_filter($colCuts);
        $carats = array_filter($colCarats);
        $clarity = array_filter($colClarity);
        $colors = array_filter($colColors);
        $prices = array_filter($colCosts, function ($v) {
            $v = preg_replace('~[^\d.]+~', '', $v);

            return is_numeric($v);
        });

        $data = array_intersect_key($data, $cuts, $carats, $clarity, $colors, $prices);
        unset($colCuts, $colCarats, $colClarity, $colColors, $colCosts, $cuts, $carats, $clarity, $colors, $prices);

        // преобразуем столбцы
        array_walk($data, function (&$val) {
            // столбец cut
            $val['A'] = trim($val['A']);
            // столбец carat
            $v = trim($val['B']);
            $v = preg_replace(['!\s+!', '!,!'], ['', '.'], $v);
            preg_match('!([\d.,-]+)!', $v, $match);
            $v = ArrayHelper::getValue($match, 1, '');
            if (mb_strlen($v, 'UTF-8') > 0) {
                $v = strtr($v, ['-' => ' - ']);

                $v = preg_replace_callback('~\d+~', function ($matches) { return floatval($matches[0]); }, $v);
            }
            $val['B'] = (mb_strlen($v, 'UTF-8') > 1) ? "{$v} CT." : false;
            // столбец clarity
            $val['C'] = trim($val['C']);
            // столбец color
            $val['D'] = trim($val['D']);
            // столбец cost
            $v = preg_replace('~[^\d.]+~', '', trim($val['E']));
            $v = floatval($v);
            $v += (mt_rand(-$v, $v) / $v) * ($v * $this->distribution / 100);
            $val['E'] = $v;
            // столбец date
            $v = trim($val['F']);
            $v = preg_replace(['~\s+~', '~[/\\,]~'], ['', '.'], $v);
            try {
                $dt = (mb_strpos($v, '-', 0, 'UTF-8') === false) ? new \DateTime($v) : \DateTime::createFromFormat('m-d-y', $v);
                $dt->setTime(0, 0, 0);
                $val['F'] = $dt->getTimestamp();
            } catch (\Exception $e) {
                // неправильная дата
                $dl = (new \DateTime())->setTime(0, 0, 0);
                $val['F'] = $dl->getTimestamp();
            }
        });
        $data = array_filter($data, function ($v) { return !empty($v['B']); });

        if (!count($data)) {
            return false;
        }

//print '<pre>'; print_r($data);
//        exit;
        $cutsR = DiamondCut::find()->all();
        $cutsA = ArrayHelper::getColumn($cutsR, 'name');
        $cutsU = array_unique(ArrayHelper::getColumn($data, 'A'), SORT_STRING);
        $cutsN = array_diff($cutsU, $cutsA);
        if (count($cutsN) > 0) {
            // есть новые значения
            $cutsG = $cutsN;
            array_walk($cutsG, function (&$val) { $val = [$val]; });
            $cutsG = array_values($cutsG);
            \Yii::$app->db->createCommand()->batchInsert('diamond_cut', ['name'], $cutsG)->execute();

            // поскольку нужны id всех новых строк, приходится делать повторный запрос
            $cutsR = DiamondCut::find()->all();
        }
        $cutsB = ArrayHelper::map($cutsR, 'name', 'id');

        $caratR = DiamondCarat::find()->all();
        $caratsA = ArrayHelper::getColumn($caratR, 'name');
        $caratsU = ArrayHelper::getColumn($data, 'B');
        $caratsU = array_unique($caratsU, SORT_STRING);
        $caratsN = array_diff($caratsU, $caratsA);
        if (count($caratsN) > 0) {
            // если новые значения
            $caratsG = $caratsN;
            array_walk($caratsG, function (&$val) { $val = [$val]; });
            $caratsG = array_values($caratsG);
            \Yii::$app->db->createCommand()->batchInsert('diamond_carat', ['name'], $caratsG)->execute();
            $caratR = DiamondCarat::find()->all();
        }
        $caratsB = ArrayHelper::map($caratR, 'name', 'id');

        $clarityR = DiamondClarity::find()->all();
        $clarityA = ArrayHelper::getColumn($clarityR, 'label');
        $clarityU = array_unique(ArrayHelper::getColumn($data, 'C'), SORT_STRING);
        $clarityN = array_diff($clarityU, $clarityA);
        if (count($clarityN) > 0) {
            // если новые значения
            $clarityG = $clarityN;
            array_walk($clarityG, function (&$val) { $val = [$val]; });
            $clarityG = array_values($clarityG);
            \Yii::$app->db->createCommand()->batchInsert('diamond_clarity', ['label'], $clarityG)->execute();
            $clarityR = DiamondClarity::find()->all();
        }
        $clarityB = ArrayHelper::map($clarityR, 'label', 'id');

        $colorR = DiamondColor::find()->all();
        $colorsA = ArrayHelper::getColumn($colorR, 'label');
        $colorsU = array_unique(ArrayHelper::getColumn($data, 'D'), SORT_STRING);
        $colorsN = array_diff($colorsU, $colorsA);
        if (count($colorsN) > 0) {
            // если новые значения
            $colorsG = $colorsN;
            array_walk($colorsG, function (&$val) { $val = [$val]; });
            $colorsG = array_values($colorsG);
            \Yii::$app->db->createCommand()->batchInsert('diamond_color', ['label'], $colorsG)->execute();
            $colorR = DiamondColor::find()->all();
        }
        $colorsB = ArrayHelper::map($colorR, 'label', 'id');

        $dl = ArrayHelper::getColumn($data, 'F');
        $dl = array_shift($dl);
        DiamondPrice::deleteAll(['date' => $dl]);

        $prices = [];
        foreach ($data as $key => $line) {
            $prices[ $key ] = [
                'diamond_cut_rid'     => $cutsB[ $line['A'] ],
                'diamond_carat_rid'   => $caratsB[ $line['B'] ],
                'diamond_clarity_rid' => $clarityB[ $line['C'] ],
                'diamond_color_rid'   => $colorsB[ $line['D'] ],
            ];
        }

        $prices = array_unique($prices, SORT_REGULAR);

        foreach ($data as $key => $line) {
            if (isset($prices[ $key ])) {
                $prices[ $key ]['cost'] = $line['E'];
            }
            if (isset($prices[ $key ])) {
                $prices[ $key ]['date'] = $dl;
            }
        }

        if (count($prices) > 0) {
            $is = [
                'diamond_cut_rid',
                'diamond_carat_rid',
                'diamond_clarity_rid',
                'diamond_color_rid',
                'date',
                'cost',
            ];
            sort($is, SORT_STRING);
            array_walk($prices, function (&$val) { ksort($val, SORT_STRING); });

            \Yii::$app->db->createCommand()->batchInsert(DiamondPrice::tableName(), $is, $prices)->execute();

            $this->insertDate = $dl;
        }

        return true;
    }

    public function unload($date) {
        $dt = new \DateTime();
        if ($date > 0) {
            $dt->setTimestamp($date);
        }

//        $this->date = $dt->format('d F Y');

        return true;
    }

    public function beforeValidate() {
        $this->file = UploadedFile::getInstance($this, 'file');

        return parent::beforeValidate();
    }

    public function afterValidate() {
        /** @var UploadedFile $file */
        $file = $this->file;
        if (!is_null($file)) {
            $path = \Yii::getAlias('@app/files/import-data');
            $path .= '/' . date('d-F-Y');
            FileHelper::createDirectory($path);

            $filePath = $path . '/' . $file->getBaseName() . '.' . $file->getExtension();

            $file->saveAs($filePath);
            $this->_filePath = realpath($filePath);
        }

        parent::afterValidate();
    }

}