<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use app\modules\admin\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model {
    const SCENARIO_REG = 'registration';
    const SCENARIO_AUTH = 'authentificate';

    public $username;
    public $password;
    public $email;
    public $rememberMe = false;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password', 'email'], 'required'],
            // email must be like email
            ['email', 'email'],
            // unique user login
            ['email' ,'unique', 'targetClass' => User::className(), 'on' => self::SCENARIO_REG , ],
            ['username' ,'unique', 'targetClass' => User::className(), 'on' => self::SCENARIO_REG , ],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // in password 6 characters minimal
            ['password', 'string', 'min' => 6],
            // password is validated by validatePassword()
            ['password', 'validatePassword', 'on' => self::SCENARIO_AUTH],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_AUTH] = ['username' , 'password', 'rememberMe'];
        $scenarios[self::SCENARIO_REG] = ['username', 'password', 'email',];

        return $scenarios;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            $dt = new \DateTime();
            $dt->add(new \DateInterval('P1D'));
            $dt->setTime(2, 0, 0);

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? ($dt->getTimestamp() - time()) : 0);
        }

        return false;
    }

    public function signUp(){
        $user = new User(['scenario' => User::SCENARIO_REG]);
        $user->attributes = [
            'username' => $this->username,
            'email' => $this->email,
        ];
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save();
    }
}
