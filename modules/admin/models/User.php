<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 13:03
 */

namespace app\modules\admin\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {
    const SCENARIO_REG = 'registration';

    public static function findIdentity($id) {
        return self::findOne(['id' => $id, 'status' => 10]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => 10]);
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function rules() {
        return [
            ['status', 'integer',],
            ['status', 'default', 'value' => 10],
            [['username' , 'password_hash', 'email', 'status',], 'required'],
            ['username' ,'string', 'min' => 3, 'max' => 25],
            ['password_hash', 'string',],
            ['email', 'email',],
            ['email', 'unique', ],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] = [];
        $scenarios[self::SCENARIO_REG] = ['username', 'password_hash', 'email', 'auth_key', 'status'];

        return $scenarios;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function setPassword($password) {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey() {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public function validatePassword($password) {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
}