<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.10.2016
 * Time: 13:14
 */

namespace app\modules\admin\models;


use app\models\AR\DiamondPrice;
use yii\data\ActiveDataProvider;

class DiamondCollectionSearch extends DiamondPrice{

    public function search( $params ){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' =>$query,
        ]);

        if ( !$this->load($params) || !$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere();
    }
}