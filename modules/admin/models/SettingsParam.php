<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 15:14
 */

namespace app\modules\admin\models;

use app\models\AR\Settings;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SettingsParam extends Model{
    protected $_model;
    protected $_params;

    public function init() {
        parent::init();

        $this->initialize();
    }

    public function rules(){
        return [
            [['limit_users_min', 'limit_users_max'], 'integer'],
        ];
    }

    protected function initialize(){
        $this->_model=Settings::find()->indexBy('key')->all();
        $this->_params = ArrayHelper::map($this->_model, 'key' , 'value');
    }

    public function saveAs($post){
        $data = $post[ self::formName() ];

        foreach($data as $key => $value){
            if (method_exists($this, "set{$key}")) $this->$key = $value;
        }
        return true;
    }

    public function getLimit_users_min(){
        return ArrayHelper::getValue($this->_params, 'limit_users_min', 0);
    }
    public function getLimit_users_max(){
        return ArrayHelper::getValue($this->_params, 'limit_users_max', 0);
    }

    public function setLimit_users_min($value){
        $value = intval($value);
        if (!is_integer($value) || $value < 0) return;

        if (ArrayHelper::keyExists('limit_users_min', $this->_params)) {
            Settings::updateAll(['value' => $value], ['key' => 'limit_users_min']);
        }else{
            $model = new Settings();
            $model->key = 'limit_users_min';
            $model->value = (string)$value;
            $model->save();
        }
    }
    public function setLimit_users_max($value){
        $value = intval($value);
        if (!is_integer($value) || $value < 0) return;

        if (ArrayHelper::keyExists('limit_users_max', $this->_params)) {
            Settings::updateAll(['value' => $value], ['key' => 'limit_users_max']);
        }else{
            $model = new Settings();
            $model->key = 'limit_users_max';
            $model->value = (string)$value;
            $model->save();
        }
    }
}