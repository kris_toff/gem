<?php
namespace app\modules\admin;

use app\components\behaviors\BMenu;
use app\modules\admin\controllers\LoginController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.09.2016
 * Time: 17:23
 */
class Module extends \yii\base\Module {
    public function init() {
        parent::init();

        $config = require(__DIR__ . '/config/adv.php');
        $handler = ArrayHelper::getValue($config, 'components.errorHandler');
        unset($config['components']['errorHandler']);

        \Yii::configure($this, $config);
        if ($handler != null) {
            \Yii::$app->getErrorHandler()->unregister();

            \Yii::$app->set('errorHandler', $handler);
            \Yii::$app->getErrorHandler()->register();
        }
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow'       => true,
                    'controllers' => ['admin/login', 'admin/registration', 'admin/lick'],
                    'roles'       => ['?'],
                ],
                [
                    'allow' => true,
                    'controllers' => ['admin/login'],
                    'actions' => ['out'],
                    'roles' => ['@']
                ],
                [
                    'allow'       => false,
                    'controllers' => ['admin/login', 'admin/registration'],
                    'roles'       => ['@'],
                ],
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }
}