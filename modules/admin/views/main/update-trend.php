<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.10.2016
 * Time: 13:37
 */
use yii\bootstrap\Html;
//use dosamigos\datepicker\DatePicker;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\bootstrap\Alert;

/** @var \yii\web\View $this */
/** @var \app\modules\admin\models\DiamondCollection $model */
/** @var integer $date */
/** @var boolean $isNewRecord */
?>


<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['trend'], ['class' => 'btn btn-success']) ?>

        <?php if ($date > 0): ?>
            <?= Html::a('Delete collection', [
                'delete-trend',
                'date' => $date
            ], ['class' => 'btn btn-danger']); ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">

        <?php if (!$isNewRecord): ?>
            <?php Alert::begin([
                'id'      => 'warning-update-price',
                'options' => [
                    'class' => 'alert-warning',
                ],
            ]) ?>

            After press "Apply" all entries in there period previously will be deleted!

            <?php Alert::end() ?>
        <?php endif; ?>

        <?php $form = ActiveForm::begin([
            'id'      => 'load-trend-file',
//    'action' => Url::toRoute(['load-trend-file']),
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]) ?>

        <? /*=
$form->field($model, 'date')->widget(DatePicker::className(), [
// DatePicker::widget([
//    'model' => $model,
//    'attribute' => 'date',
    'template' => '{addon}{input}',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'dd MM yyyy',
    ],
]) */ ?>

        <?= $form->field($model, 'file')->widget(FileInput::className(), [
            'options'       => ['accept' => 'application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            'pluginOptions' => [
                'uploadAsync' => false,

                'showPreview' => false,
                'showCaption' => true,
                'showRemove'  => true,
                'showUpload'  => false,
            ],
        ]) ?>

        <?= $form->field($model, 'distribution', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">%</span></div>',
            'inputOptions' => [
                'placeholder' => 'Imprecisiopn of price',
            ]
        ])->textInput() ?>

        <?= Html::submitButton('Apply', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>