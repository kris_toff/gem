<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.10.2016
 * Time: 2:02
 */
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProviderSlider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Add slide', ['update-slider-image'], ['class'=> 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProviderSlider,
            'columns'      => [
                'id',
                [
                    'attribute' => 'src',
                    'value'     => function ($model) {
                        return Html::img($model->src, ['class' => 'img-responsive']);
                    },
                    'format'    => 'html',
                ],
                [
                    'attribute' => 'small_text',
                ],
                'big_text',
                [
                    'label'  => 'button',
                    'value'  => function ($model) {
                        return empty($model->url) ? 'no' : 'to url: ' . Html::a($model->url, [$model->url]);
                    },
                    'format' => 'html',
                ],
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template'   => "{update}\n{delete}",
                    'urlCreator' => function ($action, $key, $index) {
                        return Url::to(["$action-slider-image", 'id' => $index]);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>

<?php
$this->registerCss(<<<CSS
.grid-view td{
word-break: break-all;
word-wrap: break-word;
}
.grid-view td > img{
max-width: 60px;
}
CSS
);