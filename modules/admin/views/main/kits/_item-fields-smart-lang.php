<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.10.2016
 * Time: 15:20
 */

/** @var \yii\bootstrap\ActiveForm $form */
/** @var \app\models\AR\SettingsSlider $model */
/** @var integer $langId */

?>


<?= $form->field($model, "translations[$langId][small_text]")->textInput()->label($model->getAttributeLabel('small_text')) ?>
<?= $form->field($model, "translations[$langId][big_text]")->textInput()->label($model->getAttributeLabel('big_text')) ?>

