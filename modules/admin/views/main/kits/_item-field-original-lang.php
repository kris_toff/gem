<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.10.2016
 * Time: 15:14
 */

/** @var \yii\bootstrap\ActiveForm $form */
/** @var \app\models\AR\SettingsSlider $model */
?>

<?= $form->field($model, 'small_text')->textInput() ?>
<?= $form->field($model, 'big_text')->textInput() ?>
