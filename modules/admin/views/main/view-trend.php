<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.10.2016
 * Time: 12:21
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;
use app\models\AR\DiamondPrice;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var integer $date */
/** @var string $month */
/** @var integer $year */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['trend'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Delete records', ['delete-trend', 'date' => $date], ['class' => 'btn btn-danger']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'closeButton' => false,
            'body' => "List of prices for month <strong>{$month}</strong> and year <strong>{$year}</strong>",
        ]) ?>
    </div>
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => \yii\grid\SerialColumn::className(),
                ],
                [
                    'attribute' => 'diamond_cut_rid',
                    'value' => function($model){
                        return $model->cut->name;
                    }
                ],
                [
                    'attribute' => 'diamond_carat_rid',
                    'value' => function($model){
                        return $model->carat->name;
                    }
                ],
                [
                    'attribute' => 'diamond_clarity_rid',
                    'value' => function($model){
                        return $model->clarity->label;
                    }
                ],
                [
                    'attribute' => 'diamond_color_rid',
                    'value' => function($model){
                        return $model->color->label;
                    }
                ],
                'cost',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => "{delete}",
                    'urlCreator' => function($action, $model, $key, $index){
                        return Url::toRoute(["$action-trend-item", 'id' => $key]);
                    }
                ]
            ]
]) ?>
    </div>
</div>