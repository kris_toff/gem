<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 14:54
 */

use app\components\AlertWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\admin\models\SettingsParam $model */
?>
<div class="row">
    <div class="col-sm-12">
        <?= AlertWidget::widget() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin([
            'layout' => 'inline',
        ]); ?>

        <?= Html::label('Range of count users in main page') ?>

        <?= $form->field($model, 'limit_users_min', [
            'template' => '<div class="input-group"><span class="input-group-addon">min</span>{input}</div>'
        ])->textInput([
            'placeholder' => 'min users'
        ]); ?>

        <?= $form->field($model, 'limit_users_max', [
            'template' => '<div class="input-group"><span class="input-group-addon">max</span>{input}</div>',
        ])->textInput([
            'placeholder' => 'max users',
        ]) ?>

        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 0;']); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>

