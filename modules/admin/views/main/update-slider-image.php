<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 16:54
 */

use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\file\FileInput;

/** @var \app\models\AR\SettingsSlider $model */
/** @var array $languages */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['slider'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
            <?= Html::a('Delete record', ['delete-slider-image', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]) ?>

        <?php
        $items = [
            [
                'label'   => 'Original English',
                'content' => $this->render('kits/_item-field-original-lang', ['model' => $model, 'form' => $form]),
            ],
        ];

        foreach ($languages as $langId => $lang) {
            $items[] = [
                'label'   => $lang,
                'content' => $this->render('kits/_item-fields-smart-lang', [
                    'model'  => $model,
                    'form'   => $form,
                    'langId' => $langId,
                ]),
            ];
        }
        ?>

        <?= TabsX::widget([
            'position'         => TabsX::POS_ABOVE,
            'align'            => TabsX::ALIGN_LEFT,
            'containerOptions' => [
                'data-enable-cache' => false,
            ],
            'pluginOptions'    => [
                'enableCache' => false,

            ],
            'items'            => $items,
        ]) ?>

        <?= $form->field($model, 'url')->textInput() ?>

        <?= $form->field($model, 'image')->widget(FileInput::className(), [
            'options'       => ['accept' => 'image/*'],
            'pluginOptions' => [
                'initialPreview'       => empty($model->src) ? [] : [
                    \Yii::getAlias('@web' . $model->src),
                ],
                'initialPreviewAsData' => true,

                'uploadAsync' => false,
                'showUpload'  => false,
                'showRemove'  => false,
            ],
        ]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>

    </div>
</div>

