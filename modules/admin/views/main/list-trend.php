<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.10.2016
 * Time: 2:39
 */
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Add new collection', ['update-trend'], ['class'=> 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [

                [
                    'class' => \yii\grid\SerialColumn::className(),
                ],
                [
                    'attribute' => 'date',
                    'format' => ['date', 'php:d F Y'],
                ],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'urlCreator' => function($action, $model){
                        return Url::toRoute(["$action-trend", 'date' => $model->date, ]);
                    }
                ]
            ]
]) ?>
    </div>
</div>