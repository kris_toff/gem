<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 16:20
 */

use kartik\tabs\TabsX;
use yii\grid\GridView;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new video', ['update-item'], ['class' => 'btn btn-success pull-right']); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'id',
                'name',
                'label',
                [
                    'attribute' => 'tab_id',
                    'value'     => function ($model) {
                        return $model->tab->label;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'format'    => ['date', 'php:H:i d-M-Y'],
                ],
                [
                    'attribute' => 'link',
                    'value'     => function ($model) {
                        /** @var \app\models\AR\Video $model */
                        return $model->linkYoutube('Youtube video');
                    },
                    'format'    => 'html',
                ],
                [
                    'class'    => 'yii\grid\ActionColumn',
                    'template' => "{update}\n{delete}",
                    'urlCreator' => function($action, $key, $index){
                        return Url::toRoute(["$action-item", 'id' => $index]);
                    }
                ],
            ],
        ]); ?>
    </div>
</div>