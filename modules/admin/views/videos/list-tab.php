<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 3:02
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new tab', ['update-tab'], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'label',
                [
                    'attribute' => 'active',
                    'value' => function($model){
                        return ($model->active == 1) ? 'yes' : 'no';
                    }
                ],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => "{update}\n{delete}",
                    'urlCreator' => function($action, $key, $index){
                        return Url::to(["$action-tab", 'id' => $index]);
                    }
                ]
            ]
        ]) ?>
    </div>
</div>
