<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 17:41
 */

/** @var \yii\web\View $this */
/** @var \app\models\AR\Video $model */
/** @var array $tabs */

use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['videos/'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
        <?= Html::a('Delete record', ['delete-item' ,'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'video-form',
            'enableClientValidation' => true,
        ]);

        print $form->field($model, 'tab_id')->dropDownList($tabs);

        print $form->field($model, 'name')->textInput();
        print $form->field($model, 'label')->textInput();


        print $form->field($model, 'link', [
            'inputTemplate' => '<div class="input-group"><span class="input-group-addon">https://www.youtube.com/watch?v=</span>{input}</div>',
            'enableClientValidation' => false,
        ])->textInput();


        print Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary btn-block']);
        ActiveForm::end();
        ?>
    </div>
</div>
