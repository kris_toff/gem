<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 3:10
 */

use yii\bootstrap\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\bootstrap\Html;

/** @var \app\models\AR\VideoTabs $model */

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['list-tabs'] , ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
        <?= Html::a('Delete record', ['delete-tab' , 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-video-tab',
        ]); ?>

        <?= $form->field($model, 'label')->textInput() ?>

        <?= $form->field($model, 'active')->widget(SwitchInput::className(), [
            'type' => SwitchInput::CHECKBOX,
        ]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>

