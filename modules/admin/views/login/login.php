<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 16:52
 */

/** @var \app\modules\admin\models\LoginForm $model */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>



<?php $form = ActiveForm::begin([
    'id' => 'admin-authorization',
    'layout'      => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4 col-md-3',
            'offset' => 'col-sm-offset-4 col-md-offset-3',
            'wrapper' => 'col-sm-8 col-md-9',
            'error' => '',
            'hint' => '',
        ]
    ],
    'options'     => [
//        'class' => 'simple-form',
    ],
]); ?>

<?= $form->field($model, 'username')->textInput(['autofocus' => true]); ?>
<?= $form->field($model, 'password')->passwordInput(); ?>

<div class="row">
    <div class="col-md-8 col-sm-6">
        <?= Html::submitButton(\Yii::t('app', 'Get to work'), ['class' => 'btn btn-primary btn-block']); ?>
    </div>
    <div class="col-md-4 col-sm-6">
        <?= Html::a(\Yii::t('app', 'Registration'), ['/admin/registration/'], ['class' => 'btn btn-link center-block']); ?>
    </div>
</div>

<? ActiveForm::end(); ?>
