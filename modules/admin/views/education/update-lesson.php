<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 11:45
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;
use kartik\tabs\TabsX;

/** @var \yii\web\View $this */
/** @var \app\models\AR\Education $model */
/** @var array $categories */
/** @var array $languages */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['lessons'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
            <?= Html::a('Delete record', ['delete-lesson', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id'      => 'form-update-education',
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]) ?>

        <?= $form->field($model, 'menu_id')->dropDownList($categories) ?>

        <?php
        $items = [
            [
                'label' => 'Original English',
                'content' => $this->render('kits/_item-field-original-lang', ['model' => $model, 'form' => $form]),
            ],
        ];

        foreach($languages as $langId => $lang){
            $items[] = [
                'label' => $lang,
                'content' => $this->render('kits/_item-fields-smart-lang', ['model' => $model, 'form' => $form, 'langId' => $langId]),
            ];
        }

        print TabsX::widget([
            'position' => TabsX::POS_ABOVE,
            'align' => TabsX::ALIGN_LEFT,
            'containerOptions' => [
                'data-enable-cache' => false,
            ],
            'pluginOptions' => [
                'enableCache'=>false,

            ],
            'items' => $items,
        ])
        ?>

        <?= $form->field($model, 'main_img')->widget(FileInput::className(), [
            'options'       => ['accept' => 'image/*'],
            'pluginOptions' => [
                'initialPreview'       => empty($model->preview_image) ? [] : [
                    \Yii::getAlias('@web' . $model->preview_image),
                ],
                'initialPreviewAsData' => true,

                'uploadAsync' => false,
                'showUpload'  => false,
                'showRemove'  => false,
            ],
        ]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>

    </div>
</div>
