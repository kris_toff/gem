<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 12:18
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use dosamigos\tinymce\TinyMce;

/** @var \yii\web\View $this */
/** @var \app\models\AR\EducationMenu $model */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['list-categories'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
        <?= Html::a('Delete record', ['delete-category', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'form-education-category',
        ]) ?>

        <?= $form->field($model ,'label')->textInput() ?>

        <?= $form->field($model, 'header')->textarea(['rows' => 6])->widget(TinyMce::className(), [

            'options' => [
                'rows' => 6
            ]
        ]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>
