<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 11:12
 */

use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\web\View $this */

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new lesson', ['update-lesson'], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'id' => 'list-lessons',
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'menu_id',
                    'value' => function($model){
                        return Html::encode($model->category->label);
                    }
                ],
                'name',
                [
                    'attribute' => 'preview_text',
                    'value' => function ($model){
                        return strip_tags($model->preview_text);
                    },
                ],
                [
                    'attribute' => 'preview_image',
                    'format' => 'raw',
                    'value' => function($model){
                        return Html::img($model->preview_image, ['class' => 'img-responsive']);
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:H:i d-M-Y'],
                ],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => "{update}\n{delete}",
                    'urlCreator' => function($action, $key, $index){
                        return Url::to(["$action-lesson", 'id' => $index]);
                    }
                ]
            ]
        ]) ?>
    </div>
</div>


<?php
$this->registerCss(<<<CSS
.grid-view td > img{
max-width: 60px;
}
.grid-view td{
word-wrap: break-word;
word-break: break-all;
}
CSS
);