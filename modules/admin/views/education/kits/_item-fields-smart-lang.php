<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.10.2016
 * Time: 16:44
 */

use dosamigos\tinymce\TinyMce;

/** @var \app\models\AR\Education $model */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var integer $langId */
?>

<?= $form->field($model, "translations[$langId][name]")->textInput()->label($model->getAttributeLabel('name')) ?>
<?= $form->field($model, "translations[$langId][preview_text]")->textarea(['rows' => 6])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 3,
    ],
])->label($model->getAttributeLabel('preview_text')) ?>
<?= $form->field($model, "translations[$langId][content]")->textarea(['rows' => 6])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 6,
    ],
])->label($model->getAttributeLabel('content')) ?>
