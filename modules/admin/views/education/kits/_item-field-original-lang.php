<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.10.2016
 * Time: 16:43
 */
use dosamigos\tinymce\TinyMce;

/** @var \app\models\AR\Education $model */
/** @var \yii\bootstrap\ActiveForm $form */
?>

<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'preview_text')->textarea(['rows' => 6])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 3,
    ],
]) ?>
<?= $form->field($model, 'content')->textarea(['rows' => 6])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 6,
    ],
]) ?>
