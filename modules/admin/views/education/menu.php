<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 12:12
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create category', ['update-category'], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'label',
                'header',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => "{update}\n{delete}",
                    'urlCreator' => function($action, $key, $index){
                        return Url::to(["$action-category", 'id' => $index]);
                    }
                ]
            ]
        ]); ?>
    </div>
</div>

