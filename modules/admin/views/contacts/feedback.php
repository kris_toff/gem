<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 2:30
 */

use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'id' => 'grid-view-feedback',
            'dataProvider' => $dataProvider,
            'columns'      => [
                'id',
                'name',
                'email',
                'phone',
                'body',
                [
                    'attribute' => 'created_at',
                    'format'    => ['date', 'php:H:i d-M-Y'],
                ],
                [
                    'class'    => \yii\grid\ActionColumn::className(),
                    'template' => "{view}\n{delete}",
                    'urlCreator' => function($action, $key, $index){
                        return Url::toRoute(["$action-item", 'id'=>$index]);
                    }
                ],
            ],
        ]) ?>
    </div>
</div>

