<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 12:07
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\file\FileInput;
use kartik\switchinput\SwitchInput;

/** @var \yii\web\View $this */
/** @var \app\models\AR\ContactInfo $model */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['info'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
            <?= Html::a('Delete record', ['delete-contact-info', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ])
        ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 4])->widget(\dosamigos\tinymce\TinyMce::className(), [

            'options' => [
                'rows' => 8,
            ],
        ]) ?>

        <?= $form->field($model, 'image')->widget(FileInput::className(), [
            'options'       => ['accept' => 'image/*'],
            'pluginOptions' => [
                'initialPreview'       => empty($model->icon) ? [] : [
                    \Yii::getAlias('@web' . $model->icon),
                ],
                'initialPreviewAsData' => true,

                'uploadAsync' => false,
                'showUpload'  => false,
                'showRemove'  => false,
            ],
        ]) ?>

        <?= $form->field($model, 'active')->widget(SwitchInput::className(), [
            'pluginOptions' => [
                'onText'   => 'Yes',
                'offText'  => 'No',
                'onColor'  => 'warning',
                'offColor' => 'default',
            ],
        ]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>
