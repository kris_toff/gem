<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 08.10.2016
 * Time: 23:26
 */

use yii\bootstrap\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;

/** @var \app\models\ContactForm $model */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['contacts/'], ['class' => 'btn btn-success']); ?>
        <?= Html::a('Delete record', ['contacts/delete-item', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>

<?= Alert::widget([
    'id' => 'alert-no-update',
    'body' => 'This record cannot be edited.',
    'options' => [
        'class' => 'alert-warning',
    ]
]) ?>

<div class="row">
    <div class="col-sm-12">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'email',
                'phone',
                'body:html',
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'H:i d-M-Y'],
                ]
            ]
]) ?>
    </div>
</div>
