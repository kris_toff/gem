<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2016
 * Time: 11:52
 */

use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;

?>

    <div class="row">
        <div class="col-sm-12">
            <?= Html::a('Add new position', ['update-contact-info'], ['class' => 'btn btn-success pull-right']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns'      => [
                    'id',
                    [
                        'attribute' => 'icon',
                        'format'    => 'html',
                        'value'     => function ($model) {
                            return Html::img($model->icon, ['class' => 'img-responsive']);
                        },
                    ],
                    'text:html',
                    [
                        'attribute' => 'active',
                        'value'     => function ($model) {
                            return ($model->active == 1) ? 'yes' : 'no';
                        },
                    ],
                    [
                        'class'      => \yii\grid\ActionColumn::className(),
                        'template'   => "{update}\n{delete}",
                        'urlCreator' => function ($action, $key, $index) {
                            return Url::toRoute(["$action-contact-info", 'id' => $index]);
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>

<?php
$this->registerCss(<<<CSS
.grid-view td > img{
max-width: 60px;
}
CSS
);