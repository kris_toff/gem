<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 15:34
 */

use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\AR\MapPoints $model */

?>


<?php Modal::begin([
    'id' => 'add-new-point-modal',
    'header' =>  \Yii::t('app', 'Create new point on the map'),
    'toggleButton' => ['label' => 'NEW POINT', 'class' => 'btn btn-primary'],
    'footer' => Html::button('Close window', ['class' => 'btn btn-default', 'data-dismiss' => 'modal',]) . Html::button('Create new point', ['class' => 'btn btn-success', 'onclick' => '$("#create-point").submit();']),
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'create-point',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
            'hint' => '',
            'error' => '',
        ]
    ]
]); ?>
<?= $form->field($model, 'name')->textInput(['autofocus' => true]); ?>
<?= $form->field($model, 'tooltip')->textInput(); ?>
<?= $form->field($model, 'title')->textInput(); ?>
<?= $form->field($model, 'text')->textarea(['rows' => 4]); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'latitude')->textInput(); ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'longitude')->textInput(); ?>
    </div>
</div>

<?= Html::submitButton('Create', ['style'=> 'display:none;']); ?>
<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' =>[
        'name',
        'tooltip',
        'title',
        'text',
        'latitude',
        'longitude',
        [
            'attribute' => 'active',
            'value' => function($model){
                return ($model->active == 1) ? 'yes' : 'no';
            }
        ],
        [
            'attribute' => 'created_at',
            'format'=>['date','php:H:i d-M-Y'],
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => "{delete}",
            'urlCreator' => function($action, $key, $index){
                return Url::toRoute(["$action-point", 'id' => $index]);
            }
        ]
    ],
]) ?>
