<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 18:07
 */

use yii\bootstrap\Html;

/** @var \app\models\AR\MapPoints $model */
/** @var mixed $key значение ключа с данными */
/** @var integer $index индекс элемента данных в массиве элементов, возвращенных поставщику данных, который начинается с 0 */
/** @var \yii\widgets\ListView $widget экземпляр виджета */

?>

<div class="list-group-item">
    <div class="pull-right"><?= Html::a('delete', ['map/delete-point', 'id' => $key], ['class' => 'btn btn-danger btn-xs']); ?></div>

    <p><?= $model->name; ?></p>
    <p><?= "Latitude:{$model->latitude}; Longitude:{$model->longitude}"; ?></p>

    <div class="clearfix"></div>
</div>