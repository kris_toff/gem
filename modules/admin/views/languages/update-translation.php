<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.10.2016
 * Time: 12:41
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;

/** @var \app\models\AR\MessageApp $model */
/** @var \app\models\AR\SourceMessageApp $modelSource */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['languages/', 'lang' => $model->language], ['class' => 'btn btn-success']); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Alert::begin([
            'options' => [
                'class' => 'alert-warning',
            ],
            'closeButton' => false,
        ]) ?>

        Translation of a word <strong><?= $modelSource->message ?></strong>
        <?php Alert::end() ?>

        <?php $form = ActiveForm::begin() ?>
        <?= Html::activeHiddenInput($model, 'id') ?>
        <?= Html::activeHiddenInput($model, 'language') ?>

        <?= $form->field($model, 'translation')->textInput() ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>