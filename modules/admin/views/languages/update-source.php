<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 16:28
 */
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/** @var \app\models\AR\SourceMessageApp $model */

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back' , ['languages/'], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'message')->textInput(['autofocus' => true]) ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>