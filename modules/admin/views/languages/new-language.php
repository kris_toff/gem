<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 14:49
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \app\models\AR\Languages $model */

?>

<?= Html::a('Back', ['languages/'], ['class' => 'btn btn-success']) ?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'code') ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>

<?php ActiveForm::end() ?>
