<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 13:39
 */
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\editable\Editable;

?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new word', ['update-source'], ['class' => 'btn btn-link']) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => false,
            'columns'      => [
                'id',
                [
                    'attribute' => 'message',
                ],
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template'   => "{update}\n{delete}",
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::toRoute(["$action-source", 'id' => $model->id]);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>