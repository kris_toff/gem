<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.10.2016
 * Time: 16:38
 */

use yii\bootstrap\Nav;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;

?>

<div class="page-header"><h1>All Languages in project</h1></div>

<?php Alert::begin([
    'id' => 'about-original-language-alert',
    'options' => [
        'class' => 'alert-danger',
    ]
]) ?>
The main original language on this site is English US. If you need to add new position, please <?= Html::a('click this', ['add-new-language'], ['class' => 'alert-link']) ?>. Please notice, it would be impossible to delete the category in future!
<?php Alert::end() ?>

<?= Nav::widget([
    'items' => $langs,
    'options' => [
        'class' => 'nav-tabs nav-justified',
    ]
]) ?>

<?= $content ?>
