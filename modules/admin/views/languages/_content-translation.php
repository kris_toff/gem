<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.10.2016
 * Time: 14:19
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>

<div class="row">
    <div class="col-sm-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => false,
            'columns' => [
                'id',
                'message',
                'translation',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => "{update}\n{delete}",
                    'urlCreator' => function($action, $model, $key, $index){
                        return Url::toRoute(["$action-translation", 'id' => $key, 'lang' => $model['language']]);
                    },
                    'visibleButtons' => [
                        'delete' => function($model, $key, $index){
                            return (!is_null($model['translation']) && mb_strlen($model['translation'], 'UTF-8'));
                        }
                    ]
                ],
            ]
        ]) ?>
    </div>
</div>
