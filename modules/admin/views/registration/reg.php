<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 2:05
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \app\modules\admin\models\LoginForm $model */

?>

<?php $form = ActiveForm::begin([
    'id' => 'registrate-user',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4 col-md-3',
            'offset' => 'col-sm-offset-4 col-md-offset-3',
            'wrapper' => 'col-sm-8 col-md-9',
            'error' => '',
            'hint' => '',
        ]
    ]
]);
?>

<?= $form->field($model, 'username' )->textInput(['autofocus' => true]); ?>
<?= $form->field($model, 'email')->textInput(); ?>
<?= $form->field($model, 'password')->passwordInput(); ?>

<div class="row">
    <div class="col-md-8 col-sm-6">
        <?= Html::submitButton('Get account', ['class' => 'btn btn-primary btn-block']); ?>
    </div>
    <div class="col-md-4 col-sm-6">
        <?= Html::a('Has already account', ['/admin/login'], ['class' => 'btn btn-link center-block']); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

