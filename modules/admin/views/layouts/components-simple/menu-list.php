<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 17.09.2016
 * Time: 23:00
 */

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\Html;

/** @var string $navbar_id identificator for all component */
/** @var \yii\web\Controller $controller */
/** @var array $menu */
/** @var boolean $activeItems */
?>

<?php
NavBar::begin([
    'id'                   => $navbar_id,
    'brandLabel'           => Html::img(['/img/logo.png'], ['alt' => 'Diamond', 'class' => 'img-responsive',]),
    'brandUrl'             => \Yii::$app->homeUrl,
    'brandOptions'         => [
        'class' => 'navbar-brand',
    ],
    'options'              => [
        'class' => 'navbar-default custom-header-menu',
    ],
    'renderInnerContainer' => true,
]);
?>

<?= Nav::widget([
//    'id' => 'menu-nav',
    'options'         => [
        'class' => 'navbar-nav navbar-right',
    ],
    'activateParents' => $activeItems,
    'activateItems'   => $activeItems,
    'items'           => $menu,
]); ?>

<?php
NavBar::end();
?>
