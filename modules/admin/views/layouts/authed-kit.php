<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.10.2016
 * Time: 16:47
 */

/** @var \yii\web\View $this */
/** @var string $content */

use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use app\modules\admin\widgets\navMenu\NavMenuWidget;

?>

<?php $this->beginContent('@admin/views/layouts/kit.php'); ?>

    <div class="row">
        <div class="wrap-complex col-sm-12">
            <div class="component component-menu">
                <div<? //= data-spy="affix" data-offset-top="1" data-offset-bottom="103"?> >
                    <div class="decor">
                        <?= NavMenuWidget::widget() ?>
                    </div>
                </div>
            </div>
            <div class="component-body">
                <div class="component-wfull component">
<!--                    <div class="component">-->
                        <div class="component-area">
                            <?= $content ?>
                        </div>
<!--                    </div>-->
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>

<?php $this->endContent() ?>