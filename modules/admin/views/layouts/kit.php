<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.09.2016
 * Time: 22:42
 */

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\modules\admin\assets\AdminAsset;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
AdminAsset::register($this);

/** @var \yii\web\Controller $controller */
$controller = $this->context;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper center-block">
    <div class="wrapper-row">
        <div class="wrap">
            <div class="wrap-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="lead text-center">
                                <span class="glyphicon glyphicon-tint"></span>
                                <?= Html::a('Admin panel', ['/admin']); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <?= $content ?>
            </div>
        </div>
    </div>

    <footer class="footer">

        <?= $this->render('components-simple/menu-list.php', [
            'navbar_id'   => 'custom-footer-menu',
            'controller'  => $controller,
            'menu'        => $controller->menu,
            'activeItems' => false,
        ]); ?>

        <div class="container-fluid keep-end">
            <div class="container law-protection">
                <div class="row">
                    <div class="col-sm-5">
                        <p class="right-protected">&copy; Transparent Diamonds. All rights reserved.</p>
                    </div>
                    <div class="col-sm-7 policy-links">
                        <p>
                            <?= Html::a('Terms And Conditions', ['/terms/index'], [
                                'class'  => 'pull-right',
                                'target' => '_blank',
                            ]); ?>
                            <?= Html::a('Privacy Policy', ['/terms/policy'], [
                                'class'  => 'pull-right',
                                'target' => '_blank',
                            ]); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
