<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:34
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\file\FileInput;
use kartik\switchinput\SwitchInput;

/** @var \yii\web\View $this */
/** @var \app\models\AR\NewsTabs $model */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['list-tabs'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
        <?= Html::a('Delete record', ['delete-tab', 'id'=>$model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'update-news',
        ]); ?>

        <?= $form->field($model, 'label')->textInput() ?>
        <?= $form->field($model, 'active')->widget(SwitchInput::className(), [
            'type' => SwitchInput::CHECKBOX,
        ]); ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']); ?>
        <?php ActiveForm::end() ?>

    </div>
</div>
