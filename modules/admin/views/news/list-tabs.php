<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:15
 */

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new tab', ['update-tab'], ['class' => 'btn btn-success']); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'label',
                [
                    'attribute' => 'active',
                    'value' => function($model){
                        return boolval($model->active) ? 'yes' : 'no';
                    }
                ],
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template'   => "{update}\n{delete}",
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::toRoute(["{$action}-tab", 'id' => $key]);
                    },
                ],
            ],
        ]); ?>
    </div>
</div>

