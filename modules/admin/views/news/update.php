<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:34
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\file\FileInput;
use dosamigos\tinymce\TinyMce;
use kartik\tabs\TabsX;

/** @var \yii\web\View $this */
/** @var \app\models\AR\News $model */
/** @var \app\models\RelatedNews $modelRel */
/** @var array $categories */
/** @var array $languages */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Back to list', ['list'], ['class' => 'btn btn-success']) ?>

        <?php if (!$model->isNewRecord): ?>
            <?= Html::a('Delete record', ['delete-item', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
        $form = ActiveForm::begin([
            'id'      => 'update-news',
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

        <?= $form->field($model, 'category_id')->dropDownList($categories) ?>
        <?= $form->field($model, 'name')->hint('Only for admin') ?>

        <?php
        $items = [
            [
                'label'   => 'Original English',
                'content' => $this->render('kits/_item-field-original-lang', ['model' => $model, 'form' => $form]),
            ],
        ];

        foreach ($languages as $langId => $lang) {
            $items[] = [
                'label'   => $lang,
                'content' => $this->render('kits/_item-fields-smart-lang', [
                    'model'  => $model,
                    'form'   => $form,
                    'langId' => $langId,
                ]),
            ];
        }

        print TabsX::widget([
            'position'         => TabsX::POS_ABOVE,
            'align'            => TabsX::ALIGN_LEFT,
            'containerOptions' => [
                'data-enable-cache' => false,
            ],
            'pluginOptions'    => [
                'enableCache' => false,

            ],
            'items'            => $items,
        ])
        ?>

        <?= $form->field($model, 'preview_image')->widget(FileInput::className(), [
            'options'       => ['accept' => 'image/*'],
            'pluginOptions' => [
                'initialPreview'       => empty($model->main_image) ? [] : [
                    \Yii::getAlias('@web' . $model->main_image),
                ],
                'initialPreviewAsData' => true,

                'uploadAsync' => false,
                'showUpload'  => false,
                'showRemove'  => false,
            ],

        ]) ?>

        <?= $form->field($modelRel, 'relatedList')->textInput()->hint('Separated by a comma') ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']); ?>
        <?php ActiveForm::end() ?>

    </div>
</div>
