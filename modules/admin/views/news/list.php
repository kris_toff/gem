<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:15
 */

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new post', ['update-item'], ['class' => 'btn btn-success']); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'id',
                'name',
                [
                    'attribute' => 'category_id',
                    'value'     => function ($model) {
                        return $model->category->name;
                    },
                ],
                'title',
                [
                    'attribute' => 'main_image',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        return Html::img($model->main_image, ['class' => 'img-responsive']);
                    },
                ],
                [
                    'attribute'=>'preview',
                    'value' => function($model){
                        return strip_tags($model->preview);
                    }
                ],
                [
                    'attribute' => 'content',
                    'value' => function($model){
                        return strip_tags($model->content);
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format'    => ['date', 'php:H:i d-M-Y'],
                ],
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template'   => "{update}\n{delete}",
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::toRoute(["{$action}-item", 'id' => $key]);
                    },
                ],
            ],
        ]); ?>

    </div>
</div>

<?php
$this->registerCss(<<<CSS
.grid-view td > img{
max-width: 60px;
}
CSS
);