<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.10.2016
 * Time: 12:02
 */

use dosamigos\tinymce\TinyMce;

/** @var \app\models\AR\News $model */
/** @var \yii\bootstrap\ActiveForm $form */
?>

<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'preview')->textarea(['row' => 4])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 3,
    ]
]) ?>
<?= $form->field($model, 'content')->textarea(['row' => 8])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 6
    ]
]) ?>
