<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.10.2016
 * Time: 11:12
 */

use dosamigos\tinymce\TinyMce;

/** @var \app\models\AR\News $model */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var integer $langId */
?>

<?= $form->field($model, "translations[$langId][title]")->textInput()->label($model->getAttributeLabel('title')) ?>
<?= $form->field($model, "translations[$langId][preview]")->textarea(['row' => 4])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 3,
    ]
])->label($model->getAttributeLabel('preview')) ?>
<?= $form->field($model, "translations[$langId][content]")->textarea(['row' => 8])->widget(TinyMce::className(), [
    'options' => [
        'rows' => 6
    ]
])->label($model->getAttributeLabel('content'))?>
