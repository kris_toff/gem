<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:15
 */

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('Create new category', ['update-category'], ['class' => 'btn btn-success']); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'name',
                [
                    'attribute' => 'tab_id',
                    'value'     => function ($model) {
                        return $model->tab->label;
                    },
                ],
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template'   => "{update}\n{delete}",
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::toRoute(["{$action}-category", 'id' => $key]);
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
