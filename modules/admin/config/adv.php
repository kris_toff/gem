<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 10:45
 */

return [
    'layout'       => 'authed-kit',
    'defaultRoute' => 'contacts',
//    'defaultRoute' => 'piriform/pages',
    'aliases'      => [
        '@admin' => '@app/modules/admin',
    ],
    'components'   => [
        'errorHandler' => [
            'class'       => 'yii\web\ErrorHandler',
            'errorAction' => 'admin/lick/elbow',
        ],
    ],
    'params'       => [

    ],
];

