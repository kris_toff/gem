<?php
namespace app\modules\admin\assets;

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 13:16
 */
class AdminAsset extends \yii\web\AssetBundle{
    public $sourcePath = '@admin/web/';

    public $css = [
        'css/disperse.less',
        'css/adjustDefaultBootstrap.less',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}