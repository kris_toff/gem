<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 16:19
 */

namespace app\modules\admin\controllers;

use app\components\behaviors\BMenu;
use app\models\AR\Video;
use app\models\AR\VideoTabs;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class VideosController extends Controller {
    public $defaultAction = 'list';

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionList() {
        $provider = new ActiveDataProvider([
            'query' => Video::find()->with('tab'),
        ]);

        return $this->render('list', [
            'dataProvider' => $provider,
        ]);
    }

    public function actionListTabs() {
        $dataProvider = new ActiveDataProvider([
            'query' => VideoTabs::find(),
        ]);

        return $this->render('list-tab', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateTab($id = 0) {
        if ($id > 0) {
            $model = VideoTabs::findOne($id);
        } else {
            $model = new VideoTabs();
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()) {
            return $this->redirect(['list-tabs']);
        }

        return $this->render('update-tab', [
            'model' => $model,
        ]);
    }

    public function actionDeleteTab($id){
        VideoTabs::deleteAll(['id' => $id]);

        return $this->redirect(['list-tabs']);
    }

    public function actionUpdateItem($id = 0) {
        if ($id < 1) {
            $model = new Video();
        } else {
            $model = Video::findOne($id);
        }

        $request= \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()) {
            return $this->redirect([$this->defaultAction]);
        }

        $tabsA = VideoTabs::find()->all();
        $tabs = ArrayHelper::map($tabsA, 'id', 'label');

        return $this->render('update', [
            'model' => $model,
            'tabs'  => $tabs,
        ]);
    }

    public function actionDeleteItem($id) {
        Video::deleteAll(['id' => $id]);
        $url = ['videos'];
        $url = array_merge($url, \Yii::$app->request->getQueryParams());
        unset($url['id']);

        $this->redirect($url);
    }
}