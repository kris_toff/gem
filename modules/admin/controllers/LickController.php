<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 11:19
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use yii\web\Controller;

class LickController extends Controller{
    public function behaviors() {
        return array(
            BMenu::className(),
        );
    }

    public function beforeAction($action) {
        $this->layout = (\Yii::$app->user->isGuest) ? 'kit' : 'authed-kit';

        return parent::beforeAction($action);
    }

    public function actions() {
        return [
            'elbow' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

}