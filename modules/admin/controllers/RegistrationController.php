<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.09.2016
 * Time: 21:12
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use yii\web\Controller;
use app\modules\admin\models\LoginForm;

class RegistrationController extends Controller {
    public $defaultAction = 'sign-up';
    public $layout        = 'kit';

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionSignUp() {
        $model = new LoginForm(['scenario' => LoginForm::SCENARIO_REG]);

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->signUp())
                return $this->redirect(['/admin/piriform']);
        }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }
}