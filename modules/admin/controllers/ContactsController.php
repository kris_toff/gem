<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 2:27
 */

namespace app\modules\admin\controllers;

use app\components\behaviors\BMenu;
use app\models\AR\ContactInfo;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ContactsController extends Controller{
    public $defaultAction = 'feedback';

    public function actionFeedback(){
        $dataProvider = new ActiveDataProvider([
            'query' => ContactForm::find(),
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'updated_at' => SORT_DESC,
                ]
            ]
        ]);

        return $this->render('feedback', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewItem($id){
        $model = ContactForm::findOne($id);
        if (is_null($model)) throw new BadRequestHttpException("Record with id {$id} not found.");

        return $this->render('feedback-view', [
            'model' => $model
        ]);
    }

    public function actionDeleteItem($id){
        ContactForm::deleteAll(['id' => $id]);

        return $this->redirect([$this->defaultAction]);
    }

    public function actionInfo(){
        $dataProvider = new ActiveDataProvider([
            'query' => ContactInfo::find(),
            'pagination' => false,
        ]);

        return $this->render('info', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateContactInfo($id = 0){
        if ($id){
            $model = ContactInfo::findOne($id);
        }else{
            $model = new ContactInfo();
        }

        $request= \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()){
            return $this->redirect(['info']);
        }

        return $this->render('update-contact', [
            'model' => $model
        ]);
    }

    public function actionDeleteContactInfo($id){
        ContactInfo::deleteAll(['id' => $id]);
        return $this->redirect(['info']);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}