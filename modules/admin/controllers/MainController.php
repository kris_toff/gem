<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 14:52
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\DiamondPrice;
use app\models\AR\Languages;
use app\models\AR\Settings;
use app\models\AR\SettingsSlider;
use app\modules\admin\models\DiamondCollection;
use app\modules\admin\models\SettingsParam;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\validators\DateValidator;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class MainController extends Controller {
    public $defaultAction = 'slider';

    public function actionSlider() {
        $providerSlider = new ActiveDataProvider([
            'query' => SettingsSlider::find(),
        ]);

        return $this->render('slider', [
            'dataProviderSlider' => $providerSlider,
        ]);
    }

    public function actionLimitUsers() {
        $model = new SettingsParam();

        $request = \Yii::$app->request;
        if ($request->isPost && $model->saveAs($request->post())) {
            \Yii::$app->session->setFlash('success', 'All updates are saved successfully.');

            return $this->refresh();
        }

        return $this->render('limit', [
            'model' => $model,
        ]);
    }

    public function actionUpdateSliderImage($id = 0) {
        if ($id) {
            $model = SettingsSlider::findOne($id);
        } else {
            $model = new SettingsSlider();
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()) {
            return $this->redirect([$this->defaultAction]);
        }

        // список языков
        $langsL = \Yii::$app->getUrlManager()->languages;
        $langsL = array_diff($langsL, ['en-US']);
        $langs = Languages::find()->where(['code' => $langsL])->all();
        $langs = ArrayHelper::map($langs, 'id', 'name');

        $langCode = Languages::getCodes();
        $langCode = array_diff($langCode, ['en-US']);
        foreach($langCode as $cnum => $cd){
            $model->translations[$cnum]['small_text'] = \Yii::t('hill', $model->small_text, null, $cd);
            $model->translations[$cnum]['big_text'] = \Yii::t('hill', $model->big_text, null, $cd);
        }

        return $this->render('update-slider-image', [
            'model' => $model,
            'languages' => $langs,
        ]);
    }

    public function actionDeleteSliderImage($id) {
        SettingsSlider::deleteAll(['id' => $id]);

        return $this->redirect([$this->defaultAction]);
    }

    public function actionTrend() {
        $dataProvider = new ActiveDataProvider([
            'query'      => DiamondPrice::find()->select('date')->distinct(),
            'pagination' => [
                'pageSize'      => 20,
                'pageParam'     => 'trend-page',
                'pageSizeParam' => 'trend-per-page',
            ],
            'sort'       => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'cost' => SORT_DESC,
                    'id'   => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('list-trend', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewTrend($date) {
        if ($date < 1) throw new BadRequestHttpException('You must set timestamp what trend-line want to see.');

        $dataProvider = new ActiveDataProvider([
            'query'      => DiamondPrice::find()->where(['date' => $date])->with('cut'),
            'pagination' => [
                'pageSize'      => 400,
                'pageParam'     => 'trend-item-page',
                'pageSizeParam' => 'trend-item-per-page',
            ],
            'sort'       => [
                'defaultOrder' => [
                    'diamond_cut_rid'     => SORT_ASC,
                    'diamond_carat_rid'   => SORT_ASC,
                    'diamond_clarity_rid' => SORT_ASC,
                    'diamond_color_rid'   => SORT_ASC,
                    'cost'                => SORT_DESC,
                ],
            ],
        ]);

        $dt = (new \DateTime())->setTimestamp($date);

        return $this->render('view-trend', [
            'dataProvider' => $dataProvider,
            'date'        => $date,
            'month' => $dt->format('F'),
            'year' => $dt->format('Y'),
        ]);
    }

    public function actionUpdateTrend($date = 0) {
        $request = \Yii::$app->request;

        $model = new DiamondCollection();
        if ($request->isPost && $model->load($request->post()) && $model->validate() && $model->parse()) {
            return $this->redirect([
                'view-trend',
                'date' => $model->insertDate,
            ]);
        }
//        $model->unload($date);

        return $this->render('update-trend', [
            'model'       => $model,
            'date' => $date,
            'isNewRecord' => !boolval($date > 0),
        ]);
    }

    public function actionDeleteTrend($date) {
        DiamondPrice::deleteAll([
            'date' => $date
        ]);

        return $this->redirect(['trend']);
    }

    public function actionDeleteTrendItem($id) {
        $model = DiamondPrice::findOne($id);
        if (is_null($model))
            return $this->redirect(['trend']);

        $date = $model->date;

        $model->delete();

        return $this->redirect(['view-trend', 'date' => $date]);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}