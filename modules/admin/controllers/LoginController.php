<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.09.2016
 * Time: 21:12
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\modules\admin\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class LoginController extends Controller {
    public $defaultAction = 'in';
    public $layout = 'kit';

    public function behaviors() {
        return array(
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'out' => ['post'],
                ]
            ],
            BMenu::className(),
        );
    }

    public function actionIn(){
        $model = new LoginForm(['scenario' => LoginForm::SCENARIO_AUTH]);

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())){
            // время сессии до 2 часов ночи следующего дня
            if ($model->login()) return $this->redirect(['/admin/']);
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }


    public function actionOut() {
        \Yii::$app->user->logout();
        return $this->goHome();
    }
}