<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2016
 * Time: 15:26
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\MapPoints;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\helpers\Inflector;
use yii\web\Controller;

class MapController extends Controller {
    public $defaultAction = 'points';

    public function actionPoints() {
        $model = new MapPoints();

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
            $model->save();

            return $this->refresh();
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => MapPoints::find(),
            'pagination' => [
                'defaultPageSize' => 1,
                'pageParam'       => 'map-page',
                'pageSizeLimit'   => [10, 25],
                'pageSizeParam'   => 'map-per-page',
            ],
            'sort'       => [
                'attributes'   => [
                    /*
                     'date' => [
                        'asc'     => [
                            'created_at' => SORT_ASC,
                            'updated_at' => SORT_ASC,
                            'id'         => SORT_ASC,
                        ],
                        'desc'    => [
                            'created_at' => SORT_DESC,
                            'updated_at' => SORT_DESC,
                            'id'         => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                        'label'   => Inflector::camel2words('byDate'),
                    ],
                     */
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'sortParam'    => 'map-sort',
            ],
        ]);

        return $this->render('points', [
            'model'        => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeletePoint($id) {
        if ($id < 1 || ($point = MapPoints::findOne(['id' => $id])) == null) {
            \Yii::$app->session->setFlash('error', 'Point ID is out of the range.');

            return $this->goBack();
        }

        $point->active = 0;
        $point->save();

        return $this->redirect(['map/']);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}