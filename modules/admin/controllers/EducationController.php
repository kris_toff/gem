<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.10.2016
 * Time: 11:10
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\Education;
use app\models\AR\EducationMenu;
use app\models\AR\Languages;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class EducationController extends Controller{
    public $defaultAction = 'lessons';

    public function actionLessons(){
        $provider = new ActiveDataProvider([
            'query' => Education::find()->with('category'),
        ]);

        return $this->render('lessons', [
            'dataProvider' => $provider,
        ]);
    }

    public function actionUpdateLesson($id = 0){
        if ($id){
            $model = Education::findOne($id);
        }else{
            $model = new Education();
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()){
            return $this->redirect([$this->defaultAction]);
        }

        $categoriesA = EducationMenu::find()->all();
        $categories = ArrayHelper::map($categoriesA, 'id', 'label');

        // список языков
        $langsL = \Yii::$app->getUrlManager()->languages;
        $langsL = array_diff($langsL, ['en-US']);
        $langs = Languages::find()->where(['code' => $langsL])->all();
        $langs = ArrayHelper::map($langs, 'id', 'name');

        $langCode = Languages::getCodes();
        $langCode = array_diff($langCode, ['en-US']);
        foreach($langCode as $cnum => $cd){
            $model->translations[$cnum]['name'] = \Yii::t('hill', $model->name, null, $cd);
            $model->translations[$cnum]['preview_text'] = \Yii::t('hill', $model->preview_text, null, $cd);
            $model->translations[$cnum]['content'] = \Yii::t('hill', $model->content, null, $cd);
        }

        return $this->render('update-lesson', [
            'model' => $model,
            'categories' => $categories,
            'languages' => $langs,
        ]);
    }

    public function actionDeleteLesson($id){
        Education::deleteAll(['id' => $id]);
        return $this->redirect([$this->defaultAction]);
    }

    public function actionListCategories(){
        $provider = new ActiveDataProvider([
            'query' => EducationMenu::find(),
        ]);

        return $this->render('menu',[
            'dataProvider' => $provider
        ]);
    }

    public function actionUpdateCategory($id = 0){
        if ($id){
            $model = EducationMenu::findOne($id);
        } else{
            $model = new EducationMenu();
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()){
            return $this->redirect(['list-categories']);
        }

        return $this->render('update-category', [
            'model' => $model
        ]);
    }

    public function actionDeleteCategory($id){
        EducationMenu::deleteAll(['id' => $id]);
        return $this->redirect(['education-categories']);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}