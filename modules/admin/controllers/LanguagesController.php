<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.10.2016
 * Time: 16:30
 */

namespace app\modules\admin\controllers;

use app\components\behaviors\BMenu;
use app\models\AR\Languages;
use app\models\AR\MessageApp;
use app\models\AR\SourceMessageApp;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class LanguagesController extends Controller {
    public $defaultAction = 'list';

    public function actionList($lang = null) {
        $langsAR = Languages::find()->all();
        $langs = ArrayHelper::getColumn($langsAR, 'code');

        $activeLang = (!is_null($lang) && ArrayHelper::isIn($lang, $langs)) ? $lang : \Yii::$app->sourceLanguage;

        $langsA = ArrayHelper::toArray($langsAR, [
            Languages::className() => [
                'label' => 'name',
                'url'   => function ($model) { return Url::toRoute(['', 'lang' => $model->code]); },
            ],
        ]);

        $tblName = SourceMessageApp::tableName();
        $query = SourceMessageApp::find()->select(["$tblName.[[id]]", "$tblName.[[message]]",])->indexBy('id');

        if ($activeLang == \Yii::$app->sourceLanguage) {
            // это базовый язык. Можно создавать новые слова
            $dataProvider = new ActiveDataProvider([
                'query'      => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);

            $content = $this->renderPartial('_content-source', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            $dataProviderS = new ActiveDataProvider([
                'query'      => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $queryY = ArrayHelper::toArray($dataProviderS->getModels(), [
                SourceMessageApp::className() => [
                    'id',
                    'message',
                ],
            ]);
            $queryM = MessageApp::find()
                ->where(['language' => $activeLang, 'id' => $dataProviderS->getKeys()])
                ->indexBy('id')
                ->asArray()
                ->all();
            array_walk($queryY, function (&$val) use ($activeLang) { $val['language'] = $activeLang; });
            $querySu = array_replace_recursive($queryY, $queryM);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $querySu,
            ]);
//            print '<pre>'; print_r($queryY); print_r($queryM); print_r($querySu);exit;
            $content = $this->renderPartial('_content-translation', [
                'dataProvider' => $dataProvider,
            ]);
        }


        return $this->render('list', [
            'langs'   => $langsA,
            'content' => $content,
        ]);
    }

    public function actionUpdateSource($id = 0) {
        if ($id) {
            $model = SourceMessageApp::findOne(['id' => $id]);
        } else {
            $model = new SourceMessageApp();
        }

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->save()) {

            return $this->redirect([$this->defaultAction]);
        }

        return $this->render('update-source', [
            'model' => $model,
        ]);
    }

    public function actionDeleteSource($id) {
        SourceMessageApp::deleteAll(['id' => $id]);

        return $this->redirect([$this->defaultAction]);
    }

    public function actionUpdateTranslation($id, $lang) {
        $model = MessageApp::findOne(['id' => $id, 'language' => $lang]);
        if (is_null($model)) {
//            throw new BadRequestHttpException("Not found entry with id $id");
            $model = new MessageApp();
            $model->language = $lang;
            $model->id = $id;
        }

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([$this->defaultAction, 'lang' => $model->language]);
        } elseif ($id > 0 && !is_null($lang)) {
            print_r($model->errors);
            $modelS = SourceMessageApp::findOne(['id' => $id]);
            if (is_null($modelS)) {
                throw new BadRequestHttpException("No words with id $id");
            }

            return $this->render('update-translation', [
                'model'       => $model,
                'modelSource' => $modelS,
            ]);
        } else {
            return $this->redirect([$this->defaultAction]);
        }
    }

    public function actionDeleteTranslation($id, $lang) {
        MessageApp::updateAll(['translation' => null], ['id' => $id, 'language' => $lang]);

        return $this->redirect([$this->defaultAction, 'lang' => $lang]);
    }

    public function actionAddNewLanguage() {
        $model = new Languages();

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([$this->defaultAction]);
        }

        return $this->render('new-language', [
            'model' => $model,
        ]);
    }

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }
}