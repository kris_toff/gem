<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 17:14
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\models\AR\Languages;
use app\models\AR\News;
use app\models\AR\NewsCategory;
use app\models\AR\NewsRelated;
use app\models\AR\NewsTabs;
use app\models\RelatedNews;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller {
    public $defaultAction = 'list';

    public function behaviors() {
        return [
            BMenu::className(),
        ];
    }

    public function actionList() {
        $dataProvider = new ActiveDataProvider([
            'query'      => News::find()->with('category'),
            'pagination' => [
                'pageSizeLimit' => [50, 400],
                'pageSize'      => 100,

            ],
            'sort'       => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListTabs() {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsTabs::find(),
        ]);

        return $this->render('list-tabs', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListCategories() {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsCategory::find()->with('tab'),
        ]);

        return $this->render('list-category', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteItem($id) {
        if (!$id) {
            throw new NotFoundHttpException("Post with id $id not found.");
        }

        News::deleteAll(['id' => $id]);

        return $this->redirect([$this->defaultAction]);
    }

    public function actionDeleteCategory($id) {
        if (!$id) {
            throw new NotFoundHttpException("Category with id $id not found.");
        }

        NewsCategory::deleteAll(['id' => $id]);

        return $this->redirect(['list-categories']);
    }

    public function actionDeleteTab($id) {
        if (!$id) {
            throw new NotFoundHttpException("Tab with id $id not found.");
        }

        NewsTabs::deleteAll(['id' => $id]);

        return $this->redirect(['list-tabs']);
    }

    public function actionUpdateItem($id = 0) {
        $modelRel = new RelatedNews();
        if ($id) {
            $model = News::findOne($id);
            $modelRel->source = $id;
        } else {
            $model = new News();
        }

        $request = \Yii::$app->request;
        if ($request->isPost) {
            if (!$model->load($request->post()) || !$model->validate()) {
                // do nothing
            } elseif (!$modelRel->load($request->post()) || !$modelRel->validate()) {
                // do nothing
            } else {
                $model->save(false);
                $modelRel->save(false);
                return $this->redirect([$this->defaultAction]);
            }
        }

        $categoriesA = NewsCategory::find()->with('tab')->all();
        $categories = [];
        foreach ($categoriesA as $ct) {
            $categories[ $ct->id ] = ArrayHelper::getValue($ct, 'tab.label', '')
                . ' / '
                . ArrayHelper::getValue($ct, 'name');
        }

        // список языков
        $langsL = \Yii::$app->getUrlManager()->languages;
        $langsL = array_diff($langsL, ['en-US']);
        $langs = Languages::find()->where(['code' => $langsL])->all();
        $langs = ArrayHelper::map($langs, 'id', 'name');

        $langCode = Languages::getCodes();
        $langCode = array_diff($langCode, ['en-US']);
        foreach($langCode as $cnum => $cd){
            $model->translations[$cnum]['title'] = \Yii::t('hill', $model->title, null, $cd);
            $model->translations[$cnum]['preview'] = \Yii::t('hill', $model->preview, null, $cd);
            $model->translations[$cnum]['content'] = \Yii::t('hill', $model->content, null, $cd);
        }

        return $this->render('update', [
            'model'      => $model,
            'categories' => $categories,
            'modelRel'   => $modelRel,
            'languages' => $langs,
        ]);
    }

    public function actionUpdateCategory($id = 0) {
        if ($id) {
            $model = NewsCategory::findOne($id);
        } else {
            $model = new NewsCategory();
        }

        $request= \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()) {
            return $this->redirect(['list-categories']);
        }

        $tabsA = NewsTabs::find()->all();
        $tabs = ArrayHelper::map($tabsA, 'id', 'label');

        return $this->render('update-category', [
            'model' => $model,
            'tabs'  => $tabs,
        ]);
    }

    public function actionUpdateTab($id = 0) {
        if ($id) {
            $model = NewsTabs::findOne($id);
        } else {
            $model = new NewsTabs();
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->save()) {
            return $this->redirect(['list-tabs']);
        }

        return $this->render('update-tab', [
            'model' => $model,
        ]);
    }
}