<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.09.2016
 * Time: 12:44
 */

namespace app\modules\admin\controllers;


use app\components\behaviors\BMenu;
use app\modules\admin\models\LoginForm;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;

class PiriformController extends Controller {
    public $defaultAction = 'pages';

    public function behaviors() {
        return [
            'menu' => BMenu::className(),
        ];
    }

    public function actionPages(){
        return $this->render('pages');
    }

}