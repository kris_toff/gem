<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 16:11
 */

namespace app\modules\admin\widgets\navMenu;


use kartik\sidenav\SideNav;
use yii\bootstrap\Widget;
use yii\bootstrap\Nav;

class NavMenuWidget extends Widget {
    public $items;

    public function init() {
        $this->items = [
            /**/
            [
                'label' => \Yii::t('app', 'Languages'),
                'icon' => 'ice-lolly-tasted',
                'url'   => ['languages/list'],
            ],
            /**/
            [
                'label' => \Yii::t('app', 'Main page'),
                'url'   => '#',
                'icon'  => 'home',
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Slider'),
                        'url' => ['main/slider'],
                        'icon' => 'expand'
                    ],
                    [
                        'label' => \Yii::t('app', 'limit-users'),
                        'url' => ['main/limit-users'],
                        'icon' => 'compressed'
                    ],
                    [
                        'label' => \Yii::t('app', 'Trend'),
                        'url' => ['main/trend'],
                        'icon' => 'stats',
                    ]
                ]
            ],
            [
                'label' => \Yii::t('app', 'Education'),
                'url'   => '#',
                'icon'  => 'education',
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Categories'),
                        'url' => ['education/list-categories'],
                        'icon' => 'book'
                    ],
                    [
                        'label' => \Yii::t('app', 'List of lessons'),
                        'url' => ['education/lessons'],
                        'icon' => 'hourglass',
                    ]
                ]
            ],
            [
                'label' => \Yii::t('app', 'News'),
                'url'   => '#',
                'icon'  => 'tasks',
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Tabs'),
                        'url' => ['news/list-tabs'],
                        'icon' => 'blackboard',
                    ],
                    [
                        'label' => \Yii::t('app', 'Categories'),
                        'url' => ['news/list-categories'],
                        'icon' => 'folder-open',
                    ],
                    [
                        'label' => \Yii::t('app', 'List of news'),
                        'url' => ['news/list'],
                        'icon' => 'comment',
                    ],

                ]
            ],
            [
                'label' => \Yii::t('app', 'Diamond map'),
                'url'   => ['map/points'],
                'icon'  => 'map-marker',
            ],
            [
                'label' => \Yii::t('app', 'Videos'),
                'url'   => '#',
                'icon'  => 'film',
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Tabs'),
                        'url' => ['videos/list-tabs'],
                        'icon' => 'blackboard'
                    ],
                    [
                        'label' => \Yii::t('app', 'List of videos'),
                        'url'=> ['videos/list'],
                        'icon' => 'facetime-video'
                    ]
                ]
            ],
            [
                'label' => \Yii::t('app', 'Contacts'),
                'url'   => '#',
                'icon'  => 'envelope',
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Feedback'),
                        'url'   => ['contacts/feedback'],
                        'icon'  => 'envelope',
                    ],
                    [
                        'label' => \Yii::t('app', 'Info'),
                        'url' => ['contacts/info'],
                        'icon' => 'list-alt'
                    ]
                ],
            ],
            [
                'label' => \Yii::t('app', 'Get out'),
                'url' => ['login/out'],
                'icon' => 'log-out',
            ]
        ];
    }

    public function run() {
        $controller = \Yii::$app->controller;
        $route = $controller->module->id . '/' . $controller->id . '/';
        if ($controller->defaultAction != $controller->action->id) $route .= $controller->action->id;

        return SideNav::widget([
            'containerOptions' => ['id' => 'left-affix-menu'],
            'type'             => SideNav::TYPE_WARNING,
            'heading'          => 'Site manager',
            'encodeLabels'     => true,
            'items'            => $this->items,
//            'route'            => $route,
            'activateItems'    => true,
            'activateParents'  => true,
        ]);
    }
}