<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news_category`.
 */
class m160928_175640_create_news_category_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('news_category', [
            'id'       => $this->primaryKey(),
            'tab_id'   => $this->integer()->notNull(),
            'name'     => $this->string()->notNull(),
//            'url_hash' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('fk_tab_id', '{{news_category}}', 'tab_id', '{{news_tabs}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('news_category');
    }
}
