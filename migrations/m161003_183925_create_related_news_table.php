<?php

use yii\db\Migration;

/**
 * Handles the creation for table `related_news`.
 */
class m161003_183925_create_related_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_related', [
            'id' => $this->primaryKey(),
            'source_news_id' => $this->integer()->notNull(),
            'related_news_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('iu_source_related', 'news_related', ['source_news_id', 'related_news_id'], true);

        $this->addForeignKey('fk_source_id', 'news_related', 'source_news_id', 'news', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_related_id', 'news_related', 'related_news_id', 'news', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_related');
    }
}
