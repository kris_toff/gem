<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news_tabs`.
 */
class m160928_154118_create_news_tabs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_tabs', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
//            'url_hash' => $this->string()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
        ]);

//        $this->createIndex('iu_hash_news', '{{news_tabs}}', '[[url_hash]]', true);

        $this->insert('{{news_tabs}}', [
            'label' => 'Overview',
//            'url_hash' => 'overview',
            'active' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_tabs');
    }
}
