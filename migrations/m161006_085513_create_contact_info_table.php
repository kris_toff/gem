<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contact_info`.
 */
class m161006_085513_create_contact_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contact_info', [
            'id' => $this->primaryKey(),
            'icon' => $this->string(),
            'text' => $this->string()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contact_info');
    }
}
