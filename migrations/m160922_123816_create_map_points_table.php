<?php

use yii\db\Migration;

/**
 * Handles the creation for table `map_points`.
 */
class m160922_123816_create_map_points_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('map_points', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Выводится только для аднимистратора'),
            'tooltip' => $this->string(),
            'title' => $this->string(),
            'text' => $this->string()->notNull(),
            'latitude' => $this->float()->notNull(),
            'longitude' => $this->float()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('map_points');
    }
}
