<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings`.
 */
class m161005_115715_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull(),
            'value' => $this->string(),
        ]);

        $this->createIndex('iu_key', 'settings', 'key', true);

        $this->createTable('settings_slider', [
            'id' => $this->primaryKey(),
            'src' => $this->string()->notNull(),
            'big_text' => $this->string(),
            'small_text' => $this->string(),
            'url' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_slider');
        $this->dropTable('settings');
    }
}
