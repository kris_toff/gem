<?php

use yii\db\Migration;

/**
 * Handles the creation for table `menu`.
 */
class m160914_082227_create_menu_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('menu', [
            'id'     => $this->primaryKey(),
            'title'  => $this->string(),
            'url'    => $this->string(),
            'position' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
        ]);

        $this->createIndex('iu_position', 'menu', 'position', true);

        $this->batchInsert('menu', ['title', 'url', 'position'], [
            ['Home', '/main', 1],
            ['Diamond Software', '/diamond-software', 2],
            ['Features', '/features', 3],
            ['Diamond Map', '/diamond-map', 4],
            ['Education', '/education', 5],
            ['News', '/news', 6],
            ['Video', '/video', 7],
            ['Contact us', '/contacts', 8],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('menu');
    }
}
