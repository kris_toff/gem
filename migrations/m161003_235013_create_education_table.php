<?php

use yii\db\Migration;

/**
 * Handles the creation for table `education`.
 */
class m161003_235013_create_education_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('education', [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'preview_text' => $this->text()->notNull(),
            'preview_image' => $this->string(),
            'content' => $this->text()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'created_at'=> $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('education_menu', [
            'id' =>$this->primaryKey(),
            'label' => $this->string()->notNull(),
            'header' => $this->text(),
        ]);

        // по какой-то причине ошибки при создании
//        $this->addForeignKey('fk_author_id', '{{education}}', 'author_id', '{{user}}', 'id', 'CASCADE', 'CASCADE');
//        $this->addForeignKey('fk_menu_id', '{{education}}', 'menu_id', '{{education_menu}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('education_menu');
        $this->dropTable('education');
    }
}
