<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video_tabs`.
 */
class m160929_071607_create_video_tabs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('video_tabs', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
//            'url_hash' => $this->string()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
        ]);

//        $this->createIndex('iu_url_hash', '{{video_tabs}}', 'url_hash', true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('video_tabs');
    }
}
