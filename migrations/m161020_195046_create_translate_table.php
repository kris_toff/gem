<?php

use yii\db\Migration;

/**
 * Handles the creation for table `translate`.
 */
class m161020_195046_create_translate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%languages}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(16)->notNull(),
            'name' => $this->string()->notNull(),

        ], $tableOptions);

        $this->createIndex('idx_code_languages', '{{%languages}}', 'code', true);


        $this->createTable('{{%source_message_hill}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull(),
            'message' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_category_source_message', '{{%source_message_hill}}', 'category', false);
        $this->createIndex('uidx_category_message_source_message', '{{%source_message_hill}}', ['category', '[[message]](255)']);

        $this->createTable('{{%message_hill}}', [
            'id' => $this->integer()->notNull(),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_message_id_language', '{{%message_hill}}', ['id', 'language']);
        $this->createIndex('idx_language_message', '{{%message_hill}}', 'language');
        $this->addForeignKey('fk_id_message_hill', '{{%message_hill}}', 'id', '{{%source_message_hill}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_language_message_hill', '{{%message_hill}}', 'language', '{{%languages}}', 'code', 'CASCADE', 'CASCADE');



        $this->createTable('{{%source_message_app}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull(),
            'message' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_category_source_message', '{{%source_message_app}}', 'category');
        $this->createIndex('uidx_category_message_source_message', '{{%source_message_app}}', ['category', '[[message]](255)']);

        $this->createTable('{{%message_app}}', [
            'id' => $this->integer()->notNull(),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_message_id_language', '{{%message_app}}', ['id', 'language']);
        $this->createIndex('idx_language_message', '{{%message_app}}', 'language');
        $this->addForeignKey('fk_id_message_app', '{{%message_app}}', 'id', '{{%source_message_app}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_language_message_app', '{{%message_app}}', 'language', '{{%languages}}', 'code', 'CASCADE', 'CASCADE');


        // insert
        $this->insert('{{%languages}}', ['code' => 'en-US', 'name' => 'English']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_id_message_app', '{{%message_app}}');
        $this->dropForeignKey('fk_language_message_app', '{{%message_app}}');
        $this->dropTable('{{%message_app}}');
        $this->dropTable('{{%source_message_app}}');

        $this->dropForeignKey('fk_id_message_hill', '{{%message_hill}}');
        $this->dropForeignKey('fk_language_message_hill', '{{%message_hill}}');
        $this->dropTable('{{%message_hill}}');
        $this->dropTable('{{%source_message_hill}}');

        $this->dropTable('{{%languages}}');
    }
}
