<?php

use yii\db\Migration;

/**
 * Handles the creation for table `diamond_cut`.
 */
class m160914_212958_create_diamonds_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('diamond_cut', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->createTable('diamond_carat', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->createTable('diamond_clarity', [
            'id' => $this->primaryKey(),
            'label' => $this->string()
        ]);

        $this->createTable('diamond_color', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
        ]);

        // основная таблица с ценами на алмазы, включает зависимости по остальным параметрам
        $this->createTable('diamond_price', [
            'id' => $this->primaryKey(),
            'diamond_cut_rid' => $this->integer()->notNull(),
            'diamond_carat_rid' => $this->integer()->notNull(),
            'diamond_clarity_rid' => $this->integer()->notNull(),
            'diamond_color_rid' => $this->integer()->notNull(),
            'cost' => $this->float(4)->notNull(),
            'date' => $this->integer(),
        ]);

        // индексация столбцов
        $this->createIndex('iu_price_ids', 'diamond_price', ['diamond_cut_rid', 'diamond_carat_rid', 'diamond_clarity_rid', 'diamond_color_rid', 'date'], true);

        // внешние ключи к таблице
        $this->addForeignKey('fk_price_cut', 'diamond_price', 'diamond_cut_rid', 'diamond_cut', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_carat', 'diamond_price', 'diamond_carat_rid', 'diamond_carat', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_clarity', 'diamond_price', 'diamond_clarity_rid', 'diamond_clarity', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_color', 'diamond_price', 'diamond_color_rid', 'diamond_color', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('diamond_price');
        $this->dropTable('diamond_color');
        $this->dropTable('diamond_clarity');
        $this->dropTable('diamond_carat');
        $this->dropTable('diamond_cut');
    }
}
