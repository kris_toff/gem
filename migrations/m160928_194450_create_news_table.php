<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news`.
 */
class m160928_194450_create_news_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('news', [
            'id'          => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name'        => $this->string()->notNull()->comment('Только для админ-панели'),
            'title'       => $this->string()->notNull(),
            'main_image'  => $this->string(),
            'preview'     => $this->string()->notNull()->comment('Короткое описание статьи при выводе в списке'),
            'content'     => $this->text()->notNull()->comment('Полное содержимое статьи'),
            'author_id'   => $this->integer()->notNull(),
            'views'       => $this->integer()->defaultValue(0),
            'created_at'  => $this->integer()->notNull(),
            'updated_at'  => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_category_id', '{{news}}', 'category_id', '{{news_category}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_author_id', '{{news}}', 'author_id', '{{user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('news');
    }
}
