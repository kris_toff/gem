<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video`.
 */
class m160929_100402_create_video_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('video', [
            'id'         => $this->primaryKey(),
            'tab_id'     => $this->integer(),
            'name'       => $this->string()->comment('Отображается только у администратора'),
            'label'      => $this->string()->notNull(),
            'link'       => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

//        $this->addForeignKey('fk_tab_id', 'video', 'tab_id', 'video_tabs', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('video');
    }
}
