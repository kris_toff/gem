/**
 * Created by Planess group on 27.09.2016.
 */

$("#switch-graph-group").on('click', 'button', function () {
    var btn = $(this);

    btn.parent().children('.btn').removeClass('active');

    $.ajax('graphics/generate-line', {
        method: "POST",
        timeout: 4000,
        cache: false,
        data: {cut: btn.data('type')},
        dataType: 'json'
    })
        .done(function (data) {
            $("#chart-graph-frame").find("iframe").remove();

            var chartJS_diamondChartGraph = new Chart($('#' + data.id), data.config);
            $("#graph-title").text(data.title);

            btn.addClass('active');
        });

});

$("#calculate-price-btn").mousedown(function () {
    var data = {};
    var isWrong = false;

    var panel = $("#panel-handle-diamond-price");
    var buttons = panel.find(".choise-group");

    buttons.each(function (index, element) {
        var selecter = $(this);

        var hint = parseInt(selecter.val(), 10);
        if (!hint) {
            selecter.popover('show');
            isWrong = true;
        } else {
            data[selecter.attr("name")] = hint;
        }
    });

    if (isWrong) {
        panel.on('focus', '.choise-group', function () {
            $(this).popover('hide');
        });
        return false;
    }

    $("#switch-graph-group").find(".btn").removeClass("active");

    $.ajax('graphics/generate-line', {
        method: "POST",
        timeout: 4000,
        cache: false,
        data: {cut: $("#diamond_cut_rid").val(),
            carat: $("#diamond_carat_rid").val(),
            clarity: $("#diamond_clarity_rid").val(),
            color: $("#diamond_color_rid").val()},
        dataType: 'json'
    })
        .done(function (data) {
            $("#chart-graph-frame").find("iframe").remove();

            var chartJS_diamondChartGraph = new Chart($('#' + data.id), data.config);
            $("#graph-title").text(data.title);

            $("#switch-graph-group").find(".btn[data-type='1']").addClass('active');
        });

    $.ajax("price/calculate", {
        method: 'POST',
        data: data,
        dataType: 'json'
    })
        .done(function (data) {
            $("#diamond-today-price").text(data.price);
        })
        .fail(function( jqXHR, textStatus, errorThrown ){
            console.log(jqXHR);
            $("#diamond-today-price").text(jqXHR.responseJSON.message);
        });

});
