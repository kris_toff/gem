/**
 * Created by Planess group on 18.09.2016.
 */

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({html: true, delay: {show: 500, hide: 100}});
});
