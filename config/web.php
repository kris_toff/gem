<?php

$params = require(__DIR__ . '/params.php');
$db = file_exists(__DIR__ . '/db-local.php') ? require(__DIR__ . '/db-local.php') : require(__DIR__ . '/db.php');

$config = [
    'id'             => 'gems',
    'basePath'       => dirname(__DIR__),
    'bootstrap'      => [
        'log',
    ],
    'homeUrl'        => '/',
    'name'           => 'Gems',
    'defaultRoute'   => 'main/index',
    'layout'         => '@app/views/layouts/simple-trend.php',
    'language'       => 'en-US',
    'sourceLanguage' => 'en-US',
    'modules'        => [
        'admin' => 'app\modules\admin\Module',
    ],
    'components'     => [
        'request'      => [
            'cookieValidationKey' => 'jBp_r_ivSu7ruodhfNC1J8J_lYaH5Nmw',
            'baseUrl'             => '/',
        ],
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\modules\admin\models\User',
            'enableAutoLogin' => true,
            'loginUrl'        => ['/admin/login'],
        ],
        'authManager'  => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'           => $db,
        /**/
        'urlManager'   => function () {
            $languages = \app\models\AR\Languages::find()->select('code')->distinct()->column();

//            array_walk($languages, function (&$val) {
//                $posd = mb_strpos($val, '-', 0, 'UTF-8');
//                $val = mb_substr($val, 0, ($posd !== false) ? $posd : null, 'UTF-8');
//            });
//print_r($languages);exit;
            return new \codemix\localeurls\UrlManager([
                'enablePrettyUrl'           => true,
                'showScriptName'            => false,
                'enableStrictParsing'       => false,
                'enableDefaultLanguageUrlCode' => true,
                'baseUrl'                   => '/',
                'rules'                     => [],
                'languages'                 => $languages,
                'ignoreLanguageUrlPatterns' => [
                    '#img/#'  => '#img/#',
                    '#css/#'  => '#css/#',
                    '#js/#'   => '#js/#',
                    '#admin#' => '#admin#',
                ],
            ]);
        },
        /*/
        'urlManager'   => [
            'class'                     => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl'           => true,
            'showScriptName'            => false,
            'enableStrictParsing'       => false,
            'baseUrl'                   => '/',
            'rules'                     => [
            ],
            'languages'                 => [
                'en',
                'ru',
                'ua',
            ],
            'ignoreLanguageUrlPatterns' => [
                '#img/#'  => '#img/#',
                '#css/#'  => '#css/#',
                '#js/#'   => '#js/#',
                '#admin#' => '#admin#',
            ],
        ],
        /**/
        'assetManager' => [
            'appendTimestamp' => true,
            'converter'       => YII_ENV_DEV ? require(__DIR__ . '/converter-local.php') : require(__DIR__
                . '/converter.php'),
            'bundles'         => [
                /**/
                'yii\web\JqueryAsset'            => [
                    'sourcePath' => null,
                    'js'         => [
                        '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js',
                    ],
                ],/**/
                'yii\widgets\PjaxAsset'          => [
                    'basePath' => '@webroot',
                    'baseUrl'  => '@web',
                    'js'       => ['js-helper/jquery.pjax.adjust.3.0.js'],
                ],
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key'      => 'AIzaSyA9X2uD0qDzmw-765vqLPy00GMq63uS9VQ',
                        'language' => 'en',
                        'version'  => '3.1.18',
                    ],
                ],
            ],
        ],
        'i18n'         => [
            'translations' => [
                'app'  => [
                    'class'              => 'yii\i18n\DbMessageSource',
                    'messageTable'       => '{{%message_app}}',
                    'sourceMessageTable' => '{{%source_message_app}}',
                    'sourceLanguage'     => 'en-US',
                ],
                'hill' => [
                    'class'              => 'yii\i18n\DbMessageSource',
                    'messageTable'       => '{{%message_hill}}',
                    'sourceMessageTable' => '{{%source_message_hill}}',
                    'sourceLanguage'     => 'en-US',
                ],
            ],
        ],
    ],
    'params'         => $params,
    'version'        => '1.0.1',
    'timeZone'       => 'Europe/Kiev',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
