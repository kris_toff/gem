<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 1:54
 */

return [
    'class' => 'yii\web\AssetConverter',
    'commands' => [
        'less' => ['css', 'lessc {from} {to} --no-color'],
        'sass' => ['css', 'sass {from} {to}'],
        'scss' => ['css', 'sass {from} {to}'],
    ],
];
?>