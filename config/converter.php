<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2016
 * Time: 1:58
 */

// Yii extension from https://github.com/nizsheanez/yii2-asset-converter
return [
    'class'=> 'nizsheanez\assetConverter\Converter',
    'force'=> true, // true : If you want convert your sass each time without time dependency
    'destinationDir' => 'css', //at which folder of @webroot put compiled files
    'parsers' => [
        'sass' => [ // file extension to parse
            'class' => 'nizsheanez\assetConverter\Sass',
            'output' => 'css', // parsed output file type
            'options' => [
//                'cachePath' => '@app/runtime/cache/sass-parser' // optional options
            ],
        ],
        'scss' => [ // file extension to parse
            'class' => 'nizsheanez\assetConverter\Sass',
            'output' => 'css', // parsed output file type
            'options' => [] // optional options
        ],
        'less' => [ // file extension to parse
            'class' => 'nizsheanez\assetConverter\Less',
            'output' => 'css', // parsed output file type
            'options' => [
                'auto' => true, // optional options
            ]
        ]
    ]
];
?>