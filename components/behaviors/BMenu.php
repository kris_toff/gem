<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.09.2016
 * Time: 19:58
 */

namespace app\components\behaviors;


use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\AR\Menu;

class BMenu extends Behavior {
    public $menu;

    public function events() {
        return [
            Controller::EVENT_BEFORE_ACTION => 'getMenu',
        ];
    }

    public function getMenu() {
        $model = Menu::find()->where(['active' => 1])->orderBy(['position' => SORT_ASC])->all();
        $this->menu = ArrayHelper::toArray($model, [
            Menu::className() => [
                'label' => function($model){ return \Yii::t('app', $model->title); },
                'url'   => function ($ml) {
                    return [$ml->url];
                },
            ],
        ]);
    }
}