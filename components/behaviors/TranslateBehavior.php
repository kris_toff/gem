<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.10.2016
 * Time: 15:35
 */

namespace app\components\behaviors;


use app\models\AR\Languages;
use app\models\AR\MessageHill;
use app\models\AR\SourceMessageHill;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class TranslateBehavior extends Behavior {
    public $fields;

    protected $codes;
    protected $category = 'hill';

    public function init() {
        parent::init();

        $this->codes = Languages::getCodes();
    }

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateTranslations',
            ActiveRecord::EVENT_AFTER_INSERT => 'insertTranslations',
        ];
    }

    public function insertTranslations() {
        foreach ($this->fields as $attr) {
            $text = $this->owner->$attr;
            if (empty($text))
                continue;

            $model = SourceMessageHill::findOne(['category' => $this->category, 'message' => $text]);
            if (is_null($model)) {

                $model = new SourceMessageHill();
                $model->message = $text;
                $model->save();
            }

            $this->updateMessages($attr, $model->id);
        }
    }


    public function updateTranslations($e) {
        $changedAttributes = ArrayHelper::getValue($e, 'changedAttributes', []);

        foreach ($this->fields as $attr) {
            $text = $this->owner->$attr;
            $sourceText = $text;
            if (empty($text)) {
                continue;
            }

            $text = ArrayHelper::getValue($changedAttributes, $attr, $text);
            if (empty($text)) {
                continue;
            }

            $model = SourceMessageHill::findOne(['category' => $this->category, 'message' => $sourceText]);
            if (is_null($model)) {
                $model = new SourceMessageHill();
            }
            $model->message = $text;
            $model->save();

            $this->updateMessages($attr, $model->id);
        }
    }

    protected function updateMessages($attr, $sourceId){
        $translations = $this->owner->translations;

        if (!empty($translations)) {
            foreach ($translations as $langcode => $item) {
                $sourceText = $this->owner->$attr;
                $text = ArrayHelper::getValue($item, $attr);
                $model = MessageHill::findOne(['id' => $sourceId, 'language' => $this->codes[ $langcode ]]);

                if (!is_null($model)) {
                    // запись есть. Нужно обновить
                    $model->translation = (empty($text) && $text == $sourceText) ? null : $text;
                    $model->save();
                } elseif (!empty($text) && $sourceText != $text) {
                    $model = new MessageHill();
                    $model->id = $sourceId;
                    $model->translation = $text;
                    $model->language = $this->codes[ $langcode ];
                    $model->save();
                }
            }
        }
    }
}
