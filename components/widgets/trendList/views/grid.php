<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.10.2016
 * Time: 20:33
 */

/** @var \yii\web\View $this */
/** @var array $data */

/*

Array
(
    [1] => Array
        (
            [date] => 1472730078
            [cut] => Rounds
            [carat] => .15 - .17 CT.
            [price] => $ 703.09
            [diff] => 0
        )

    [2] => Array
        (
            [date] => 1472730078
            [cut] => Pears
            [carat] => 3.0 - 3.99 CT.
            [price] => $ 11 566.36
            [diff] => 0
        )

    [3] => Array
        (
            [date] => 1472730078
            [cut] => Princess
            [carat] => .50 - .69 CT.
            [price] => $ 1 881.82
            [diff] => 0
        )

    [4] => Array
        (
            [date] => 1472730078
            [cut] => Marquise
            [carat] => .46 - .49 CT.
            [price] => $ 1 405.45
            [diff] => 0
        )

)

 */
?>

    <div class="trend-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <?php foreach ($data as $item): ?>
                        <?= $this->render('_item', ['data' => $item]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
