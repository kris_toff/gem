<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.10.2016
 * Time: 0:04
 */

/** @var \yii\web\View $this */
/** @var array $data */
?>

<div class="col-md-3 col-sm-6">
    <div class="thumbnail">
        <div class="ver-line-box">
            <div class="ver-line"></div>
        </div>

        <div class="caption">
            <div id="trend-cut"><strong><?= $data['cut'] ?></strong><small class="text-muted"><?= ', ' . $data['carat'] ?></small></div>
            <div id="trend-diff-price"><?= sprintf('%+.4f', \Yii::$app->formatter->asDecimal($data['diff'], 4))  ?></div>
            <div id="trend-price-average"><?= $data['price'] ?></div>
            <div id="trend-update"><?= (new \DateTime())->setTimestamp($data['date'])->format('d M Y') ?></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>