<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.10.2016
 * Time: 12:14
 */
namespace app\components\widgets\trendList;

use app\models\AR\DiamondCarat;
use app\models\AR\DiamondCut;
use app\models\AR\DiamondPrice;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\base\Widget;

class TopTrendWidget extends Widget {
    protected $data;

    public function init() {
        parent::init();

        $dates = DiamondPrice::find()->select('date')->orderBy(['date' => SORT_DESC])->limit(2)->distinct()->column();
        if (!count($dates)) {
            $this->data = null;

            return;
        }

        $data = (new Query())->select(['average' => 'AVG([[cost]])', 'date', 'diamond_cut_rid', 'diamond_carat_rid'])
            ->from('diamond_price')
            ->where(['date' => $dates])
            ->groupBy(['date', 'diamond_cut_rid', 'diamond_carat_rid'])
            ->having(['>', 'COUNT(*)', 0])
            ->all();

        $cuts = DiamondCut::find()->select('name')->indexBy('id')->column();
        $carats = DiamondCarat::find()->select('name')->indexBy('id')->column();

        foreach ($data as $item) {
            $ni = $item;
            $ni['cut'] = \Yii::t('app', $cuts[ $item['diamond_cut_rid'] ]);
            $ni['carat'] = $carats[ $item['diamond_carat_rid'] ];
            unset($ni['diamond_cut_rid'], $ni['diamond_carat_rid']);

            $mir[ $item['date'] ][ $item['diamond_cut_rid'] ][ $item['diamond_carat_rid'] ] = $ni;
        }

        if (!isset($mir) || count($mir) < 2) {
            $this->data = null;

            return;
        }
        $data = $mir;
        $mir = array_values($mir);

        $st = array_pop($mir);
        $ft = array_pop($mir);

        $mir = [];

        foreach ($data as $key => &$item) {
            $item = array_intersect_key($item, $st, $ft);

            array_walk($item, function (&$val, $key) use ($st, $ft) {
                $val = array_intersect_key($val, ArrayHelper::getValue($st, $key, []), ArrayHelper::getValue($ft, $key, []));
            });

            foreach ($item as $k => $i) {
                $carat_id = array_rand($i);
                $mir[ $k ] = $i[ $carat_id ];
                unset($mir[ $k ]['average']);

                $ls_n = $data[ max(array_keys($data)) ][ $k ][ $carat_id ]['average'];
                $fr_n = $data[ min(array_keys($data)) ][ $k ][ $carat_id ]['average'];
                $mir[ $k ]['price'] = '$ ' . number_format($ls_n, 2, '.', ' ');

                $diff = ($ls_n - $fr_n);
                $mir[ $k ]['diff'] = ($diff > 0) ? '+' . $diff : $diff;
            }
        }

        $this->data = $mir;
    }

    public function run() {
        if (is_null($this->data))
            return '';

        return Html::tag('div', $this->render('grid', ['data' => $this->data]), ['class' => 'diamond-trend-feed']);
    }
}

