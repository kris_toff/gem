<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.10.2016
 * Time: 17:16
 */

namespace app\components\widgets;

use yii\bootstrap\Html;
use app\models\AR\Languages;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;

class LanguageDropDown extends Dropdown {
    private static $_labels;
    private        $_isError;

    public function init() {
        $route = \Yii::$app->controller->route;
        $appLanguage = \Yii::$app->language;
        $this->_isError = $route === \Yii::$app->getErrorHandler()->errorAction;
        $params = \Yii::$app->request->get();

        array_unshift($params, '/' . $route);
//print '<pre>'; print $route; print_r($params);exit;
        foreach (\Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2) === '-*';
            if ($language === $appLanguage
                || // Also check for wildcard language
                $isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)
            ) {
                continue;   // Exclude the current language
            }
            if ($isWildcard) {
                $language = substr($language, 0, 2);
            }
            $params['language'] = $language;
            $this->items[] = [
                'label' => self::label($language),
                'url'   => $params,
            ];
        }
//print_r($this->items);
        parent::init();
    }

    public static function label($code) {
        if (self::$_labels === null) {
            $labels = Languages::find()->all();

            self::$_labels = ArrayHelper::map($labels, 'code', 'name');
//            self::$_labels = [
//                'de' => \Yii::t('language', 'German'),
//                'fr' => \Yii::t('language', 'French'),
//                'en' => \Yii::t('language', 'English'),
//            ];
//            print_r(self::$_labels);exit;
        }

        return isset(self::$_labels[ $code ]) ? self::$_labels[ $code ] : null;
    }

    public function run() {
        return ($this->_isError
            || count(\Yii::$app->getUrlManager()->languages)
            < 2) ? '' : Html::tag('div', Html::button(LanguageDropDown::label(\Yii::$app->language)
                . ' <span class="caret"></span>', [
                'class' => 'btn btn-default btn-sm dropdown-toggle',
                'data-toggle' => 'dropdown',
                'style' => 'padding: 5px 15px;',
            ]) . "\r\n" . parent::run(), [
            'class' => 'btn-group navbar-right',
            'style' => 'margin-top: 11px; margin-left: 15px;',
        ]);
    }
}