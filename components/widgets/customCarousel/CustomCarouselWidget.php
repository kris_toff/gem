<?php

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 12:00
 */

namespace app\components\widgets\customCarousel;

use dosamigos\gallery\Carousel;
use yii\bootstrap\Html;
use app\components\widgets\customCarousel\assets\CarouselAsset;

class CustomCarouselWidget extends Carousel{

    public function renderTemplate(){
        $template[] = '<div class="slides"></div>';
        $template[] = '<h3 class="title"></h3>';
        $template[] = '<div class="content"></div>';
        $template[] = '<a class="prev">‹</a>';
        $template[] = '<a class="next">›</a>';
        $template[] = '<a class="play-pause"></a>';
        $template[] = '<ol class="indicator"></ol>';

        return Html::tag('div', implode("\n", $template), $this->templateOptions);
    }

    public function registerClientScript() {
        $view = $this->getView();
        CarouselAsset::register($view);

        parent::registerClientScript();
    }
}