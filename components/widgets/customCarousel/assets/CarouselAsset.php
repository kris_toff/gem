<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2016
 * Time: 12:19
 */

namespace app\components\widgets\customCarousel\assets;


use yii\web\AssetBundle;

class CarouselAsset extends AssetBundle{

    public $sourcePath = '@app/components/widgets/customCarousel/web';

    public $js = [

        'js/carousel-adds.js',
    ];

    public $css = [
        'css/carousel-adds.less',
    ];

    public $depends = [
        'dosamigos\gallery\GalleryAsset',
        'dosamigos\gallery\DosamigosAsset',
    ];

    public $publishOptions =[
        'forceCopy' => true,
    ];
}