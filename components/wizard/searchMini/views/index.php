<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 15:40
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var string $title */
/** @var string|array $url */
/** @var \app\models\SearchModule $model */
/** @var array $params */
?>


    <div class="outer-space">
        <div class="simple-block-header"><strong><?= $title ?></strong></div>

        <?php
        $form = ActiveForm::begin([
            'id'          => 'search-module',
            'action'      => $url,
            'method'      => 'get',
            'fieldConfig' => [
                'enableLabel'      => false,
                'validateOnBlur'   => false,
                'validateOnChange' => false,
                'validateOnType'   => false,
            ],
        ]);
        ?>

        <?php foreach ($params as $key => $val): ?>
            <?= Html::hiddenInput($key, $val) ?>
        <?php endforeach; ?>

        <?= $form->field($model, 'text', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-btn">'
                . Html::submitButton('<span class="glyphicon glyphicon-search"></span>', ['class' => 'btn btn-secondary'])
                . '</span></div>',
        ])->textInput([
            'placeholder' => \Yii::t('app', 'Enter search terms'),
            '',
        ]) ?>

        <?php ActiveForm::end() ?>
    </div>

<?php
$this->registerCss(<<<CSS
.outer-space{
    padding:25px;
    border: 1px solid #ecf0f2;
}
.input-group-btn button{
    background-color: transparent;
    border:1px solid #ccc;
    border-left:none;
}
CSS
);