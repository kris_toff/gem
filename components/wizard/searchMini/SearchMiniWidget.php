<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.10.2016
 * Time: 15:38
 */

namespace app\components\wizard\searchMini;

use app\models\SearchModule;
use yii\bootstrap\Widget;
use yii\helpers\Url;

class SearchMiniWidget extends Widget {
    public $title;
    public $url;
    public $model;

    protected $params;

    public function init() {
        parent::init();

        if ($this->model == null) {
            $this->model = new SearchModule();
        }

        if ($this->title == null)
            $this->title = 'Search';
        $this->title = \Yii::t('app', $this->title);
        $url = $this->url ?: Url::toRoute('');

        if (is_array($url)) {
            $params = array_slice($url, 1, null, true);
            $params = array_diff_key($params, ['tab' => 1]);

            $url = array_shift($url);
        } else {
            $params = [];
        }
        $queryParams = array_diff_key(\Yii::$app->request->getQueryParams(), [$this->model->formName() => 1]);
        $params = array_merge($params, $queryParams);

        $this->url = $url;
        $this->params = $params;
    }

    public function run() {
        return $this->render('index', [
            'model'  => $this->model,
            'title'  => $this->title,
            'url'    => $this->url,
            'params' => $this->params,
        ]);
    }
}