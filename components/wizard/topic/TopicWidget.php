<?php

namespace app\components\wizard\topic;

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.10.2016
 * Time: 2:02
 */

use yii\bootstrap\Widget;
use yii\bootstrap\Html;

class TopicWidget extends Widget{
    public $title;

    public function init() {
        $this->id = 'page-clou';

        parent::init();

        $this->title = \Yii::t('app', $this->title);

        print Html::beginTag('div', $this->options);
    }

    public function run() {
        print $this->render('page', [
            'title' => $this->title
        ]);
        print Html::endTag('div');
    }
}