<?php

/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 14:10
 */

namespace app\components\wizard\categoriesUnit;

use yii\bootstrap\Widget;

class CategoryWidget extends Widget{
    public $items;

    public function run(){
        return $this->render('list', ['items' => $this->items]);
    }
}