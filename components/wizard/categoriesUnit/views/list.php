<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2016
 * Time: 14:37
 */

use yii\bootstrap\Html;

?>

<div class="category-list">
    <div class="category-list-header"><strong><?= \Yii::t('app', 'Categories') ?></strong></div>

    <?php print Html::beginTag('div', ['class' => 'list-group']); ?>

    <?php foreach ($items as $item): ?>

        <?php print Html::a(Html::tag('span', $item['badge'], ['class' => 'badge'])
            . Html::tag('span', '', ['class' => 'glyphicon glyphicon-chevron-right'])
            . ' '
            . $item['label'], $item['url'], ['class' => 'list-group-item']); ?>

    <?php endforeach; ?>

    <?php print Html::endTag('div'); ?>
</div>
